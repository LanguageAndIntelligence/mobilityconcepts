"""
Author: Thanh Thieu

Assist in analyzing results from multiple sequence tagger.
"""

import os
import pickle
from matplotlib import pyplot as plt
import pandas as pd
from collections import defaultdict
import numpy as np

import evaluation
import config
import m_utils
import entities
import attributes


def combineResults():
    """
    Combine results from multiple models into an Excel file.

    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"

    file_paths = list()
    for fold in range(1, 6):
        file_paths.append(_resultFileCRF(dataset_name, "test", True, fold, "exact"))
    for fold in range(1, 6):
        file_paths.append(_resultFileNeuroNER(dataset_name, "pubmed", "test", False, fold, "exact"))
    for fold in range(1, 6):
        file_paths.append(_resultFileEnsemble(dataset_name, "Softmax", 5, fold, "exact", 'test'))
    summary_file = os.path.join(base_path, "comp_best_models.csv")
    evaluation.combineEval2CSV(file_paths, summary_file)


def combineAverageResults():
    """
    Combine results from multiple models into an Excel file.
    Compute average (mean & standard deviation) of all folds.

    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"
    cross_val = 'test'

    for matching_style in ['exact', 'partial']:
        all_avg_files = list()  # to be written to a summary file
        for method in ['CRF', 'LSTM_Wiki', 'LSTM_Pubmed',
                       'Ensemble_Mob', 'Ensemble_Act', 'Ensemble_Asst', 'Ensemble_Quant', 'Ensemble_ScDef']:
            # Note: Ensemble performs best with difference sizes of feature window on diffrent entity types
            for sent_split in [True, False]:
                if method.lower().startswith('ensemble'):
                    window_size = -1
                    # Note: no difference between sent vs doc; there's only 1 ensemble result
                    if not sent_split:
                        continue  # bypass one of the two options for sent_split, so that ensemble model only run once

                # Read performance of all folds
                perfs = list()
                for fold in range(1, 6):
                    if method == 'CRF':
                        f = open(_resultFileCRF(dataset_name, cross_val, sent_split, fold, matching_style), "rb")
                    elif method == 'LSTM_Wiki':
                        f = open(_resultFileNeuroNER(dataset_name, "wiki", cross_val, sent_split, fold, matching_style), "rb")
                    elif method == 'LSTM_Pubmed':
                        f = open(_resultFileNeuroNER(dataset_name, "pubmed", cross_val, sent_split, fold, matching_style), "rb")
                    elif method == 'Ensemble_Mob':
                        multiclass = "ECOC"
                        window_size = 29
                        f = open(_resultFileEnsemble(dataset_name, multiclass, window_size, fold, matching_style, cross_val), "rb")
                    elif method == 'Ensemble_Act':
                        multiclass = "ECOC"
                        window_size = 23
                        f = open(_resultFileEnsemble(dataset_name, multiclass, window_size, fold, matching_style, cross_val), "rb")
                    elif method == 'Ensemble_Asst':
                        multiclass = "ECOC"
                        window_size = 1
                        f = open(_resultFileEnsemble(dataset_name, multiclass, window_size, fold, matching_style, cross_val), "rb")
                    elif method == 'Ensemble_Quant':
                        multiclass = "ECOC"
                        window_size = 1
                        f = open(_resultFileEnsemble(dataset_name, multiclass, window_size, fold, matching_style, cross_val), "rb")
                    elif method == 'Ensemble_ScDef':
                        multiclass = "ECOC"
                        window_size = 33
                        f = open(_resultFileEnsemble(dataset_name, multiclass, window_size, fold, matching_style, cross_val), "rb")
                    else:
                        raise ValueError('Unknown model name!')

                    data = pickle.load(f)
                    perfs.append(data)
                    f.close()
                # Average performance
                avg_perf = defaultdict(lambda: defaultdict(dict))
                for ent_type in entities.allEntityTypes():
                    att = attributes.TextSpan
                    for measure in ['F1', 'precision', 'recall']:
                        all_measures = list()
                        for fold in perfs:
                            all_measures.append(fold[ent_type][att][measure])
                        measure_avg = np.mean(all_measures)
                        measure_std = np.std(all_measures)
                        # Record average
                        avg_perf[ent_type][measure]['avg'] = measure_avg
                        avg_perf[ent_type][measure]['std'] = measure_std
                # Output file name
                file_name = "avg_" + method.lower() + '_'
                if method.lower().startswith('ensemble'):
                    file_name += multiclass + '_' + str(window_size)
                else:
                    file_name += "sent" if sent_split else "doc"
                file_name += '_' + matching_style + ".csv"
                # Write out average performance
                csv_file = os.path.join(base_path, file_name)
                evaluation.writeCSV(avg_perf, csv_file)
                pkl_file = os.path.splitext(csv_file)[0] + '.pkl'
                with open(pkl_file, "wb") as f:
                    pickle.dump(dict(avg_perf), f)  # a defaultdict cannot be pickled, a dict can
                all_avg_files.append(pkl_file)

        # Write summary file
        summary_file = os.path.join(base_path, 'summary_' + matching_style + '.csv')
        evaluation.combineEval2CSV(all_avg_files, summary_file)


def _resultFileNeuroNER(dataset_name, embedding_name, crossval_name, sent_split, fold, matching_style):
    """
    Return the absolute path to a result file.

    :param dataset_name: (str)
    :param embedding_name: (str) pubmed / wiki
    :param crossval_name: (str) train / valid / test
    :param sent_split: (bool) True / False
    :param fold: (int)
    :param matching_style: (str) exact / partial
    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "neuroner", dataset_name, embedding_name)
    file_name = "eval_neuroner_" + crossval_name + "_" + embedding_name + "_" \
                + ("sent_" if sent_split else "doc_") + str(fold) + "_" + matching_style + ".pkl"
    return os.path.join(base_path, file_name)


def _resultFileCRF(dataset_name, crossval_name, sent_split, fold, matching_style):
    """
    Return the absolute path to a result file.

    :param dataset_name: (str)
    :param crossval_name: (str) train / valid / test
    :param sent_split: (bool) True / False
    :param fold: (int)
    :param matching_style: (str) exact / partial
    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "crf", dataset_name)
    file_name = "eval_crf_" + crossval_name + "_" + ("sent_" if sent_split else "doc_") + str(fold) + "_" + matching_style + ".pkl"
    return os.path.join(base_path, file_name)


def _resultFileEnsemble(dataset_name, classifier, window_size, fold, matching_style, crossval_name):
    """
    Return the absolute path to a result file.

    :param dataset_name: (str)
    :param classifier: (str) Softmax / ECOC
    :param window_size: (int) Width of the feature window
    :param fold: (int)
    :param matching_style: (str) exact / partial
    :param crossval_name: (str) should be "test"; only evaluate on the test set
    :return:
    """
    sent_split = False  # ensemble learning always work on document basis
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "ensembler", dataset_name, classifier, "window_" + str(window_size))
    file_name = "eval_ensembler_" + crossval_name + "_" + ("sent_" if sent_split else "doc_") + str(fold) + "_" + matching_style + ".pkl"
    return os.path.join(base_path, file_name)


def compareEnsembleModels():
    """
    Compare performance of ensemble models.

    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"

    all_data = defaultdict(lambda: defaultdict(dict))
    for classifier in ["Softmax", "ECOC"]:
        for window_size in range(1, 40, 2):
            data = dict()
            row_names = list()
            for fold in range(1, 6):
                result_file = _resultFileEnsemble(dataset_name, classifier, window_size, fold, "exact", 'test')
                results = pickle.load(open(result_file, 'rb'))
                measures = list()
                for ent_type in entities.allEntityTypes():
                    measures.append(results[ent_type][attributes.TextSpan]["F1"])
                    if fold == 1:
                        row_names.append('_'.join([ent_type.__name__, "F1"]))
                col_name = '_'.join(["fold", str(fold)])
                data[col_name] = measures
            data = pd.DataFrame(data, index=row_names)
            all_data[classifier][window_size] = data

    indices = row_names
    for measure_name in indices:
        plt.figure()
        for classifier in all_data:
            averages = list()
            windows_sizes = list(all_data[classifier].keys())
            for window_size in windows_sizes:
                averages.append(all_data[classifier][window_size].loc[measure_name].mean())
            idx_max = averages.index(max(averages))  # index of the max element of the list
            plt.plot(windows_sizes, averages, label=classifier + ' (window max=' + str(windows_sizes[idx_max]) + ')')

        plt.xticks(windows_sizes)
        plt.xlabel("Feature window size")
        plt.ylabel("Average F1 score on 5 folds")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["ensemble", measure_name]) + ".png"))


def compareBaseSequenceTaggers():
    """
    Compare performance of base sequence taggers including CRF and LSTM.

    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"

    # Collect CRF data
    crf_data = defaultdict(lambda: defaultdict(dict))
    for split_sent in [True, False]:
        data = dict()
        row_names = list()
        for fold in range(1, 6):
            result_file = _resultFileCRF(dataset_name, "test", split_sent, fold, "exact")
            results = pickle.load(open(result_file, 'rb'))
            measures = list()
            for ent_type in entities.allEntityTypes():
                measures.append(results[ent_type][attributes.TextSpan]["F1"])
                if fold == 1:
                    row_names.append('_'.join([ent_type.__name__, "F1"]))
            col_name = '_'.join(["fold", str(fold)])
            data[col_name] = measures
        data = pd.DataFrame(data, index=row_names)
        crf_data[split_sent] = data

    # Collect NeuroNER data
    neuroner_data = defaultdict(lambda: defaultdict(dict))
    for embedding in ['pubmed', 'wiki']:
        for split_sent in [True, False]:
            data = dict()
            row_names = list()
            for fold in range(1, 6):
                result_file = _resultFileNeuroNER(dataset_name, embedding, "test", split_sent, fold, "exact")
                results = pickle.load(open(result_file, 'rb'))
                measures = list()
                for ent_type in entities.allEntityTypes():
                    measures.append(results[ent_type][attributes.TextSpan]["F1"])
                    if fold == 1:
                        row_names.append('_'.join([ent_type.__name__, "F1"]))
                col_name = '_'.join(["fold", str(fold)])
                data[col_name] = measures
            data = pd.DataFrame(data, index=row_names)
            neuroner_data[embedding][split_sent] = data

    indices = row_names
    for measure_name in indices:
        # Plot CRF & LSTM together
        plt.figure()
        for split_sent in crf_data:
            plt.plot(range(1, 6), crf_data[split_sent].loc[measure_name], label='_'.join(['CRF', 'sent' if split_sent else 'doc']))
        for embedding in neuroner_data:
            for split_sent in neuroner_data[embedding]:
                plt.plot(range(1, 6), neuroner_data[embedding][split_sent].loc[measure_name],
                         label='_'.join(['LSTM', embedding, 'sent' if split_sent else 'doc']))
        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        plt.ylabel("F1 score")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["crf_lstm", measure_name]) + ".png"))

        # Plot CRF
        plt.figure()
        for split_sent in crf_data:
            plt.plot(range(1, 6), crf_data[split_sent].loc[measure_name], label='_'.join(['CRF', 'sent' if split_sent else 'doc']))
        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        plt.ylabel("F1 score")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["crf", measure_name]) + ".png"))

        # Plot LSTM
        plt.figure()
        for embedding in neuroner_data:
            for split_sent in neuroner_data[embedding]:
                plt.plot(range(1, 6), neuroner_data[embedding][split_sent].loc[measure_name],
                         label='_'.join(['LSTM', embedding, 'sent' if split_sent else 'doc']))
        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        plt.ylabel("F1 score")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["lstm", measure_name]) + ".png"))

        # Plot LSTM & CRF mean
        plt.figure()
        averages = list()
        model_names = list()
        for embedding in neuroner_data:
            for split_sent in neuroner_data[embedding]:
                averages.append(neuroner_data[embedding][split_sent].loc[measure_name].mean())
                model_names.append('_'.join(["LSTM", embedding, 'sent' if split_sent else 'doc']))
        for split_sent in crf_data:
            averages.append(crf_data[split_sent].loc[measure_name].mean())
            model_names.append('_'.join(["CRF", 'sent' if split_sent else 'doc']))
        plt.plot(model_names, averages)
        plt.xlabel("Sequence taggers")
        plt.xticks(rotation=30)
        plt.gcf().subplots_adjust(bottom=0.25)
        # plt.tight_layout()
        plt.ylabel("Average F1 score on 5 folds")
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["lstm_crf_avg", measure_name]) + ".png"))


def compareBestModels():
    """
    Plot comparison of best models of CRF, LSTM, and Ensemblers.
    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"

    # Collect Ensemble data
    ensemble_data = defaultdict(lambda: defaultdict(dict))
    for classifier in ["Softmax", "ECOC"]:
        for window_size in range(1, 40, 2):
            data = dict()
            row_names = list()
            for fold in range(1, 6):
                result_file = _resultFileEnsemble(dataset_name, classifier, window_size, fold, "exact", 'test')
                results = pickle.load(open(result_file, 'rb'))
                measures = list()
                for ent_type in entities.allEntityTypes():
                    measures.append(results[ent_type][attributes.TextSpan]["F1"])
                    measures.append(results[ent_type][attributes.TextSpan]["precision"])
                    measures.append(results[ent_type][attributes.TextSpan]["recall"])
                    if fold == 1:
                        row_names.append('_'.join([ent_type.__name__, "F1"]))
                        row_names.append('_'.join([ent_type.__name__, "precision"]))
                        row_names.append('_'.join([ent_type.__name__, "recall"]))
                col_name = '_'.join(["fold", str(fold)])
                data[col_name] = measures
            data = pd.DataFrame(data, index=row_names)
            ensemble_data[classifier][window_size] = data

    # Collect CRF data
    crf_data = defaultdict(lambda: defaultdict(dict))
    for split_sent in [True, False]:
        data = dict()
        row_names = list()
        for fold in range(1, 6):
            result_file = _resultFileCRF(dataset_name, "test", split_sent, fold, "exact")
            results = pickle.load(open(result_file, 'rb'))
            measures = list()
            for ent_type in entities.allEntityTypes():
                measures.append(results[ent_type][attributes.TextSpan]["F1"])
                measures.append(results[ent_type][attributes.TextSpan]["precision"])
                measures.append(results[ent_type][attributes.TextSpan]["recall"])
                if fold == 1:
                    row_names.append('_'.join([ent_type.__name__, "F1"]))
                    row_names.append('_'.join([ent_type.__name__, "precision"]))
                    row_names.append('_'.join([ent_type.__name__, "recall"]))
            col_name = '_'.join(["fold", str(fold)])
            data[col_name] = measures
        data = pd.DataFrame(data, index=row_names)
        crf_data[split_sent] = data

    # Collect NeuroNER data
    neuroner_data = defaultdict(lambda: defaultdict(dict))
    for embedding in ['pubmed', 'wiki']:
        for split_sent in [True, False]:
            data = dict()
            row_names = list()
            for fold in range(1, 6):
                result_file = _resultFileNeuroNER(dataset_name, embedding, "test", split_sent, fold, "exact")
                results = pickle.load(open(result_file, 'rb'))
                measures = list()
                for ent_type in entities.allEntityTypes():
                    measures.append(results[ent_type][attributes.TextSpan]["F1"])
                    measures.append(results[ent_type][attributes.TextSpan]["precision"])
                    measures.append(results[ent_type][attributes.TextSpan]["recall"])
                    if fold == 1:
                        row_names.append('_'.join([ent_type.__name__, "F1"]))
                        row_names.append('_'.join([ent_type.__name__, "precision"]))
                        row_names.append('_'.join([ent_type.__name__, "recall"]))
                    col_name = '_'.join(["fold", str(fold)])
                col_name = '_'.join(["fold", str(fold)])
                data[col_name] = measures
            data = pd.DataFrame(data, index=row_names)
            neuroner_data[embedding][split_sent] = data

    # Plot comparison
    for measure_name in row_names:
        plt.figure()
        plt.plot(range(1, 6), crf_data[True].loc[measure_name], label='_'.join(['CRF', 'sent']))
        plt.plot(range(1, 6), neuroner_data['pubmed'][False].loc[measure_name], label='_'.join(['LSTM', 'pubmed', 'doc']))

        if measure_name.startswith("Mobility"):
            plt.plot(range(1, 6), ensemble_data['ECOC'][29].loc[measure_name], label='Ensemble_ECOC_29')
        elif measure_name.startswith("Action"):
            plt.plot(range(1, 6), ensemble_data['ECOC'][23].loc[measure_name], label='Ensemble_ECOC_23')
        elif measure_name.startswith("Assistance"):
            plt.plot(range(1, 6), ensemble_data['ECOC'][1].loc[measure_name], label='Ensemble_ECOC_1')
        elif measure_name.startswith("Quantification"):
            plt.plot(range(1, 6), ensemble_data['ECOC'][1].loc[measure_name], label='Ensemble_ECOC_1')
        elif measure_name.startswith("ScoreDefinition"):
            plt.plot(range(1, 6), ensemble_data['ECOC'][33].loc[measure_name], label='Ensemble_ECOC_33')

        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        # plt.ylabel("")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["best", measure_name]) + ".png"))


def compareMatchingStyle():
    """
    Compare exact vs partial matching styles.

    :return:
    """
    base_path = os.path.join(config.getConfig().DATA_DIR, "results", "summary")
    m_utils.safe_mkdir(base_path)

    dataset_name = "gsc_all_ents"

    # Collect CRF data
    crf_data = defaultdict(lambda: defaultdict(dict))
    for matching_style in ['exact', 'partial']:
        for split_sent in [True, False]:
            data = dict()
            row_names = list()
            for fold in range(1, 6):
                result_file = _resultFileCRF(dataset_name, "test", split_sent, fold, matching_style)
                results = pickle.load(open(result_file, 'rb'))
                measures = list()
                for ent_type in entities.allEntityTypes():
                    measures.append(results[ent_type][attributes.TextSpan]["F1"])
                    if fold == 1:
                        row_names.append('_'.join([ent_type.__name__, "F1"]))
                col_name = '_'.join(["fold", str(fold)])
                data[col_name] = measures
            data = pd.DataFrame(data, index=row_names)
            crf_data[matching_style][split_sent] = data

    # Collect NeuroNER data
    neuroner_data = defaultdict(lambda: defaultdict(dict))
    for matching_style in ['exact', 'partial']:
        for embedding in ['pubmed', 'wiki']:
            for split_sent in [True, False]:
                data = dict()
                row_names = list()
                for fold in range(1, 6):
                    result_file = _resultFileNeuroNER(dataset_name, embedding, "test", split_sent, fold, matching_style)
                    results = pickle.load(open(result_file, 'rb'))
                    measures = list()
                    for ent_type in entities.allEntityTypes():
                        measures.append(results[ent_type][attributes.TextSpan]["F1"])
                        if fold == 1:
                            row_names.append('_'.join([ent_type.__name__, "F1"]))
                        col_name = '_'.join(["fold", str(fold)])
                    col_name = '_'.join(["fold", str(fold)])
                    data[col_name] = measures
                data = pd.DataFrame(data, index=row_names)
                neuroner_data[matching_style][embedding][split_sent] = data

    # Plot comparison
    for measure_name in row_names:
        plt.figure()
        plt.plot(range(1, 6), crf_data['exact'][True].loc[measure_name], label='_'.join(['CRF', 'sent', 'exact']))
        plt.plot(range(1, 6), crf_data['partial'][True].loc[measure_name], label='_'.join(['CRF', 'sent', 'partial']))
        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        plt.ylabel("F1 score")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["matching_styles_CRF", measure_name]) + ".png"))

        plt.figure()
        plt.plot(range(1, 6), neuroner_data['exact']['pubmed'][False].loc[measure_name], label='_'.join(['LSTM', 'pubmed', 'doc', 'exact']))
        plt.plot(range(1, 6), neuroner_data['partial']['pubmed'][False].loc[measure_name],
                 label='_'.join(['LSTM', 'pubmed', 'doc', 'partial']))
        plt.xticks(range(1, 6))
        plt.xlabel("Fold")
        plt.ylabel("F1 score")
        plt.legend()
        plt.title(measure_name)
        plt.savefig(os.path.join(base_path, '_'.join(["matching_styles_LSTM", measure_name]) + ".png"))


if __name__ == "__main__":
    combineAverageResults()
    combineResults()
    compareEnsembleModels()
    compareBaseSequenceTaggers()
    compareBestModels()
    compareMatchingStyle()
