This source code demonstrates models that identify WHO ICF's Mobility-related concepts from medical free text in electronic health records.

Program entry:

* main_ner.py: Run in console with an appropriate argument: --test-crf, --test-neuroner, --test-ensembler.
* result_analysis.py: Generate result tables and graphs. No argument.

Following dependencies are expected to be already installed. Refer to the dependency's website for installation instruction:

* Stanford CoreNLP: https://stanfordnlp.github.io/CoreNLP/ 
* Stanford CRF NER: https://nlp.stanford.edu/software/CRF-NER.html 
* NeuroNER: http://neuroner.com/ 
* GloVe: https://nlp.stanford.edu/projects/glove/ 
* Word vectors of PubMed and PMC texts: http://bio.nlplab.org/ 
* BERT: https://github.com/google-research/bert 
* BioBERT: https://github.com/dmis-lab/biobert

In addition, your system should already have fundamental programming tools including:

* Python
* Tensorflow
* SkLearn, Numpy, Scipy
* Java