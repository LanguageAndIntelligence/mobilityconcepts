import argparse
import os


class LoadFromFile(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        with values as f:
            parser.parse_args(f.read().split(), namespace)


params = None
CONFIG_FILEPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.txt")  # Look in project root directory


def getConfig():
    """
    Load configuration/parameters from a a file.
    Only load 1 time.
    :return:
    """
    global params
    if params:
        return params

    parser = argparse.ArgumentParser()

    parser.add_argument("--DATA-DIR")
    parser.add_argument("--LIB-DIR")

    parser.add_argument("--TRAIN-SUBFOLDER")
    parser.add_argument("--VALIDATION-SUBFOLDER")
    parser.add_argument("--TEST-SUBFOLDER")

    parser.add_argument("--NeuroNER-DIR")
    parser.add_argument("--NeuroNER-PARAM-DIR")

    parser.add_argument("--NeuroNER-ORIGINAL-MODELS-DIR")
    parser.add_argument("--NeuroNER-BEST-MODELS-DIR")
    parser.add_argument("--NeuroNER-TARGET-MODELS-DIR")

    parser.add_argument("--INTEGRITY-LOG-FILE")

    parser.add_argument("--TAG-DIR-CRF")
    parser.add_argument("--TAG-DIR-NeuroNER")
    parser.add_argument("--TAG-DIR-GOLD")

    parser.add_argument('--file', type=open, action=LoadFromFile)
    params = parser.parse_args(['--file', CONFIG_FILEPATH])
    return params
