"""
@Author: Thanh Thieu

Check data integrity at all levels.
Write error messages to a dedicated log text file.
"""

import logging
import re

import entities
import config

LOG_FILE = config.getConfig().INTEGRITY_LOG_FILE
logging.basicConfig(filename=LOG_FILE, filemode='w', level=logging.WARNING)


def checkEntityTextspan(docID, doc):
    """
    Check if the recorded text span of entities agrees with character indices of the entities.

    :param doc: Document
    :return: Log mis-matches to a file.
    """
    if not doc.text:
        logging.warning("Doc %s has no raw text.", docID)
        return

    for ent_type in entities.allEntityTypes():
        ents = doc.getAnnotations(ent_type)
        if ents:
            for ent in ents:
                ent_text = ent.text
                ori_text = doc.text[ent.first_char_index:ent.last_char_index]
                if ent_text != ori_text:
                    logging.warning("Doc %s ; ent %s ; %i | %i ; xml text = \"%s\" ; raw text = \"%s\"",
                                    docID, type(ent).__name__, ent.first_char_index, ent.last_char_index,
                                    ent_text, ori_text)


def checkEntityTokens(docID, ent, tokens, token_spans):
    """
    Check if the suspected tokens and token spans match the original info in the entity.

    :param docID:
    :param ent:
    :param tokens: Suspected tokens.
    :param token_spans: Suspected token spans.
    :return: Log mis-matches to a file.
    """
    if ent.tokens != tokens:
        logging.warning(
            "Token sequence mis-match: Doc %s ; ent %s ; %i | %i ; original tokens = %s ; suspected tokens = %s",
            docID, type(ent).__name__, ent.first_char_index, ent.last_char_index,
            ent.tokens, tokens)
    if not token_spans or ent.first_char_index != token_spans[0][0] or ent.last_char_index != token_spans[-1][1]:
        logging.warning("Token span mis-match: Doc %s ; ent %s ; %i | %i ; suspected token spans = %s",
                        docID, type(ent).__name__, ent.first_char_index, ent.last_char_index, token_spans)


def fixEntityLeadingTrailingSpaces(text, firstchar_idx, lastchar_idx):
    """
    If the entity text has leading or trailing spaces then trim them.

    :param ent:
    :return: New text and new first and last character indices
    """
    leading_spaces = re.search(r"^\s+", text)
    if leading_spaces:
        firstchar_idx += leading_spaces.end() - leading_spaces.start()

    trailing_spaces = re.search(r"\s+$", text)
    if trailing_spaces:
        lastchar_idx -= trailing_spaces.end() - trailing_spaces.start()

    return text.strip(), firstchar_idx, lastchar_idx
