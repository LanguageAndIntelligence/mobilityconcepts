"""
Author: Thanh Thieu

Common utilities for other modules.
"""

import sys
import re
import os
import chardet

SENTENCE_CONCAT_CHAR = '_'  # sentence concatenation character, to append a sentence ID to a document ID
CORPUS_CONCAT_CHAR = '+'  # document concatenation character, to append a parallel corpus ID to a document ID


def lookupEntityType(name):
    """
    Search for a entity type using its name.
    :param name: str
        Name of the entity type/class.
    :return: Entity class OR None
        The entity type as a class if the name matches, otherwise None.
    """
    return getattr(sys.modules["entities"], name, None)


def lookupAttributeValue(name, AttributeType):
    """
    Search the enum type for a member that has the same name.
    :param name: str
        Name of a member.
    :param AttributeType: Enum
        A enum/categorical attribute.
    :return: Enum member OR None
        A corresponding enum member matching the name, or None if cannot match the name to any enum member.
    """
    if name not in AttributeType.__members__:
        return None
    else:
        return AttributeType[name]


def extractFileID(filename):
    """
    Strip off prefixes and suffixes, usually denote the annotator's name or type of the dataset.
    :param filename: Name of the file, with extension.
    :return: file ID
    """
    fileid = os.path.splitext(filename)[0]

    idx_suffix = re.search(r"jc|jona|psh|pei-shu|peishu|jh|jamil|ln|lisa|updated", fileid.lower())
    if idx_suffix:
        idx_suffix = idx_suffix.start()
        if fileid[idx_suffix - 1] == '_' or fileid[idx_suffix - 1] == '-': idx_suffix -= 1
        fileid = fileid[:idx_suffix]

    idx_prefix = re.search(r"gsc", fileid.lower())
    if idx_prefix:
        idx_prefix = idx_prefix.end()
        if fileid[idx_prefix] == '_' or fileid[idx_prefix] == '-': idx_prefix += 1
        fileid = fileid[idx_prefix:]

    return fileid


def taggerName(full_name):
    """
    Extract name of a tagger, given the fully qualified name.

    :param full_name: (str) A qualified name such as "package.module"
    :return: (str) The module name.
    """
    last_dot_idx = full_name.rindex(".")
    return full_name[last_dot_idx + 1:]


def sentenceID(docID, sent_index):
    """
    Create a sentence ID by concatenating a document ID and a sentence index.

    :param docID:
    :param sent_index: (int)
    :return: (str)
    """
    return str(docID) + SENTENCE_CONCAT_CHAR + str(sent_index)


def parseSentenceID(sentID):
    """
    Split a sentence ID into its consituent document ID and sentence index.

    :param sentID: (str)
    :return: (str, int)
    """
    divider_pos = sentID.rindex(SENTENCE_CONCAT_CHAR)
    return sentID[:divider_pos], int(sentID[divider_pos + 1:])


def parallelDocumentID(docID, corpus_index):
    """
    Create a parallel document ID by concatenating a corpus index and an original document ID.

    :param docID:
    :param corpus_index: (int)
    :return: (str)
    """
    return str(docID) + CORPUS_CONCAT_CHAR + str(corpus_index)


def parseParallelDocumentID(docID):
    """
    Split a parallel document ID into its consituent original document ID and corpus index.

    :param docID: (str)
    :return: (str, int)
    """
    divider_pos = docID.rindex(CORPUS_CONCAT_CHAR)
    return docID[:divider_pos], int(docID[divider_pos + 1:])


def generateParallelDocumentIDs(originalDocIDs, num_parallel_corpuses):
    """
    Concatenate each document ID by several corpus IDs.

    :param originalDocIDs: (list)
    :param num_parallel_corpuses: (int)
    :return: (list)
    """
    return [parallelID for parallelID in
            [parallelDocumentID(docID, corpusID)
             for docID in originalDocIDs
             for corpusID in range(1, num_parallel_corpuses + 1)]]


def safe_mkdir(folder_path):
    """
    Create a folder tree if it does not exist.

    :param folder_path: Path to the inner most subfolder.
    :return:
    """
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


def get_file_encoding(filepath):
    """
    Sense and return encoding of a text file.
    :param filepath: (str)
    :return: (str)
    """
    with open(filepath, mode='rb') as f:
        code_table = chardet.detect(f.read())

    return code_table['encoding']


def get_sub_folders(parent_folder):
    """
    Get immediate sub-folders of a designated parent folder.
    :param parent_folder:
    :return:
    """
    return [fld for fld in os.listdir(parent_folder) if (not fld.startswith('.')) and os.path.isdir(os.path.join(parent_folder, fld))]


def get_files(parent_folder):
    """
    Get immediate files inside a parent folder.
    :param parent_folder:
    :return:

    """
    return [file for file in os.listdir(parent_folder) if os.path.isfile(os.path.join(parent_folder, file))]
