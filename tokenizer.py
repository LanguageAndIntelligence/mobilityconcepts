from nltk.tokenize import TreebankWordTokenizer
from nltk.tokenize.util import align_tokens

# from nltk.tokenize.stanford import CoreNLPTokenizer
# from nltk.parse.corenlp import CoreNLPParser
from nltk.parse.corenlp import GenericCoreNLPParser

import re


### NLTK PTB tokenizer ###

class TreebankWordTokenizer2(TreebankWordTokenizer):
    def span_tokenize(self, text):
        """
        Fix double quotations.
        :param text:
        :return:
        """
        # Fix double quotations in text
        text = text.replace('``', '"').replace("''", '"')
        # Word tokenization
        tokens = self.tokenize(text)
        # Fix double quotations in tokens
        for idx, tok in enumerate(tokens):
            if tok == "``":  # beginning double quotation
                tokens[idx] = '"'
            elif tok == "''":  # ending double quotation
                tokens[idx] = '"'
        # Match tokens to indices
        token_spans = align_tokens(tokens, text)
        return tokens, token_spans


# Shared resource
nltk_tokenizer = TreebankWordTokenizer2()


### Stanford PTB tokenizer ###

# import subprocess
# subprocess.call(['java', '-cp', '*', 'edu.stanford.nlp.sentiment.SentimentPipeline', '-file', 'foo.txt'])


class GenericCoreNLPParser2(GenericCoreNLPParser):
    # def __init__(self, url='http://localhost:9000', encoding='utf8'):
    #     super(GenericCoreNLPParser2, self).__init__(url, encoding)

    def tokenize(self, text, properties=None):
        """
        Send out only token text identity.
        """
        default_properties = {
            'annotators': 'tokenize,ssplit',

        }
        default_properties.update(properties or {})
        result = self.api_call(text, properties=default_properties)

        # Init output
        tokens = list()  # a list of tokens
        sent_bounds = list()  # sentence boundary, marked by index of the last token of the current sentence, plus 1; also serves as index of the first token of the next sentence

        # Collect output
        for sent in result['sentences']:
            tokens.extend([(tok['originalText'] or tok['word']) for tok in sent['tokens']])
            sent_bounds.append(len(sent['tokens']) + (sent_bounds[-1] if len(sent_bounds) > 0 else 0))

        return tokens, sent_bounds

    def span_tokenize(self, text, properties=None):
        """
        Send out not only token text identity but also first and last character indices of the token.
        """
        default_properties = {
            'annotators': 'tokenize,ssplit',
        }
        default_properties.update(properties or {})
        result = self.api_call(text, properties=default_properties)

        # Init output
        tokens = list()
        first_chars = list()  # index of the first character of each token
        last_chars = list()  # index of the last character of each token, plus 1
        sent_bounds = list()  # sentence boundary, marked by index of the last token of the current sentence, plus 1; also serves as index of the first token of the next sentence

        # Collect output
        for sent in result['sentences']:
            tokens.extend([(tok['originalText'] or tok['word']) for tok in sent['tokens']])
            first_chars.extend([tok["characterOffsetBegin"] for tok in sent['tokens']])
            last_chars.extend([tok["characterOffsetEnd"] for tok in sent['tokens']])
            sent_bounds.append(len(sent['tokens']) + (sent_bounds[-1] if len(sent_bounds) > 0 else 0))

        return tokens, first_chars, last_chars, sent_bounds


class CoreNLPParser2(GenericCoreNLPParser2):
    _OUTPUT_FORMAT = 'penn'
    parser_annotator = 'parse'


class CoreNLPTokenizer2(CoreNLPParser2):
    # Modify the default behavior
    adjusted_behavior = {"tokenize.options": "strictTreebank3=true,ptb3Escaping=false"}

    # adjusted_behavior = None

    def __init__(self, url='http://localhost:9000', encoding='utf8'):
        super(CoreNLPTokenizer2, self).__init__(url, encoding)

    def tokenize(self, text, properties=adjusted_behavior):
        """
        Returns tokens and sentence boundaries.
        :param text:
        :param properties:
        :return: tuple(tokens), tuple(sent_bounds)
        """
        tokens, sent_bounds = super(CoreNLPTokenizer2, self).tokenize(text, properties)
        self._postProcessDivider(tokens, [0] * len(tokens), [0] * len(tokens), sent_bounds)  # fake character indices
        self._postProcessStickingLetterDigit(tokens, [0] * len(tokens), [0] * len(tokens), sent_bounds)
        self._postProcessTrailingFullstop(tokens, [0] * len(tokens), [0] * len(tokens), sent_bounds)
        self._postProcessFailingFullstop(tokens, [0] * len(tokens), [0] * len(tokens), sent_bounds)
        if len(tokens) > 0: # an empty string doesn't have sentence boundary
            assert sent_bounds[-1] == len(tokens)  # error check
        return tuple(tokens), tuple(sent_bounds)

    def span_tokenize(self, text, properties=adjusted_behavior):
        """
        Return tokens, token spans, and sentence boundaries.
        :param text:
        :param properties:
        :return: tuple(tokens), tuple(zip(first_chars, last_chars)), tuple(sent_bounds)
        """
        tokens, first_chars, last_chars, sent_bounds = super(CoreNLPTokenizer2, self).span_tokenize(text, properties)
        self._postProcessDocLeadingSpaces(text, first_chars, last_chars)
        self._postProcessDivider(tokens, first_chars, last_chars, sent_bounds)
        self._postProcessStickingLetterDigit(tokens, first_chars, last_chars, sent_bounds)
        self._postProcessTrailingFullstop(tokens, first_chars, last_chars, sent_bounds)
        self._postProcessFailingFullstop(tokens, first_chars, last_chars, sent_bounds)
        if len(tokens) > 0: # an empty string doesn't have sentence boundary
            assert sent_bounds[-1] == len(tokens) == len(first_chars) == len(last_chars)  # error check
        return tuple(tokens), tuple(zip(first_chars, last_chars)), tuple(sent_bounds)

    def _updateSentenceBoundary(self, sent_bounds, tok_index, offset_at, offset_after):
        """
        Update the sentence boundary to sync with a post-processing  made on tokenization.
        :param sent_bounds: List of token indices that mark boundary of sentences.
        :param tok_index: Index of the token that has been post-processed.
        :param offset_at: Amount of adjustment at the post-processed token.
        NONE to signal no adjustment. A real value indicates an additional sentence boundary will be added.
        :param offset_after: Amount of adjustment after the post-processed token. NONE to signal no adjustment.
        :return:
        """
        for sent_idx in range(len(sent_bounds)):
            if sent_bounds[sent_idx] >= tok_index:
                break
        if sent_bounds[sent_idx] > tok_index:
            if offset_at is not None:
                sent_bounds.insert(sent_idx, tok_index + offset_at)
            if offset_after is not None:
                start = sent_idx + 1 if offset_at is not None else sent_idx
                for j in range(start, len(sent_bounds)):
                    sent_bounds[j] += offset_after
        elif sent_bounds[sent_idx] == tok_index:
            if offset_at is not None:
                sent_bounds.insert(sent_idx + 1, tok_index + offset_at)
            if offset_after is not None:
                start = sent_idx + 2 if offset_at is not None else sent_idx + 1
                for j in range(start, len(sent_bounds)):
                    sent_bounds[j] += offset_after
        else:  # sent_bounds[sent_idx] < tok_idx
            # This might never happen, because the last sentence boundary sits at the end of the sentence.
            if offset_at is not None:
                sent_bounds.append(tok_index + offset_at)

    def _postProcessFailingFullstop(self, tokens, first_chars, last_chars, sent_bounds):
        """
        If there is no space after a full stop the tokenizer may fail on this.
        E.g. "discuss.The"
        :param tokens:
        :param first_chars:
        :param last_chars:
        :return:
        """
        tok_idx = 0
        while tok_idx < len(tokens):  # A FOR loop doesn't work because tokens may change after each iteration
            match = re.search(r"\.[A-Z]", tokens[tok_idx])  # capture: dot, followed by a capitalized character
            if match:
                char_idx = match.start()
                tok = tokens[tok_idx]
                original_first_char = first_chars[tok_idx]
                # Adjust tokens
                del tokens[tok_idx]
                tokens.insert(tok_idx, tok[char_idx + 1:len(tok)])
                tokens.insert(tok_idx, tok[char_idx:char_idx + 1])
                tokens.insert(tok_idx, tok[0:char_idx])
                # Adjust char indices
                first_chars.insert(tok_idx + 1, original_first_char + char_idx + 1)
                first_chars.insert(tok_idx + 1, original_first_char + char_idx)
                last_chars.insert(tok_idx, original_first_char + char_idx + 1)
                last_chars.insert(tok_idx, original_first_char + char_idx)
                # Adjust sent boundaries, resulted from adding 2 more tokens
                self._updateSentenceBoundary(sent_bounds, tok_idx, 2, 2)
                # Jump token pointer
                tok_idx += 1
                # If there is a match, the number of tokens increases by 2
                # This recursively check the trailing part of the dividing token
            tok_idx += 1

    def _postProcessTrailingFullstop(self, tokens, first_chars, last_chars, sent_bounds):
        """
        Abbreviation such as "A." is not recognized by both the default Stanford tokenizer and the strict PTB tokenizer.
        :param tokens:
        :param first_chars:
        :param last_chars:
        :return:
        """
        tok_idx = 0
        while tok_idx < len(tokens):  # A FOR loop doesn't work because tokens may change after each iteration
            tok = tokens[tok_idx]
            if len(tok) > 1 and tok[-1] == '.':
                char_idx = len(tok) - 1
                original_first_char = first_chars[tok_idx]
                # Adjust tokens
                del tokens[tok_idx]
                tokens.insert(tok_idx, tok[char_idx:len(tok)])
                tokens.insert(tok_idx, tok[0:char_idx])
                # Adjust char indices
                first_chars.insert(tok_idx + 1, original_first_char + char_idx)
                last_chars.insert(tok_idx, original_first_char + char_idx)
                # Adjust sent boundaries, for the additional 1 token.
                # Notice the last sentence of a document doesn't have to have a full stop or equivalence.
                self._updateSentenceBoundary(sent_bounds, tok_idx, None if tok_idx + 2 == len(tokens) else 2, 1)
            # Increase the index normally
            tok_idx += 1

    def _postProcessStickingLetterDigit(self, tokens, first_chars, last_chars, sent_bounds):
        """
        Split tokens containing both letters and digits, such as "x400ft" -> "x", "400", "ft"
        :return:
        """
        tok_idx = 0
        while tok_idx < len(tokens):  # A FOR loop doesn't work because tokens may change after each iteration
            match = re.search(r"[a-zA-Z][0-9]|[0-9][a-zA-Z]", tokens[tok_idx])
            if match:
                char_idx = match.start() + 1
                tok = tokens[tok_idx]
                original_first_char = first_chars[tok_idx]
                # Adjust tokens
                del tokens[tok_idx]
                tokens.insert(tok_idx, tok[char_idx:len(tok)])
                tokens.insert(tok_idx, tok[0:char_idx])
                # Adjust char indices
                first_chars.insert(tok_idx + 1, original_first_char + char_idx)
                last_chars.insert(tok_idx, original_first_char + char_idx)
                # Adjust sent boundaries, for the additional 1 token
                self._updateSentenceBoundary(sent_bounds, tok_idx, None, 1)
            # Increase the index normally
            # If there is a match, the number of tokens increases by 1,
            #   thus this recursively check the second half of the dividing token
            tok_idx += 1

    def _postProcessDocLeadingSpaces(self, text, first_chars, last_chars):
        """
        Stanford tokenizer considers the first token always starts at character index 0.
        This is an issue when the document text starts with several spaces.
        This has no effect on sentence boundary marked by token index.
        :param text:
        :param first_chars: (list)
        :param last_chars: (list)
        :return: Adjust input token spans if needed.
        """
        offset = re.search(r"\S", text)  # match any non-whitespace character
        if offset and offset.start() > 0:  # the non-whitespace character is not the first character of the document
            offset = offset.start()
            for idx in range(len(first_chars)):
                first_chars[idx] += offset
                last_chars[idx] += offset

    def _postProcessDivider(self, tokens, first_chars, last_chars, sent_bounds):
        """
        If a token contains dividing characters such as "/-\|", then split it.
        E.g. "driving/transportation" -> "driving", "/", "transportation"
        :param tokens:
        :return:
        """
        tok_idx = 0
        while tok_idx < len(tokens):  # A FOR loop doesn't work because tokens may change after each iteration
            divider_indices = list()
            for iter in re.finditer(r"[a-zA-Z][/\-]+[a-zA-Z]", tokens[tok_idx]):
                divider_indices.append(iter.start() + 1)
            if divider_indices:
                tok = tokens[tok_idx]
                sub_tokens = list()
                sub_first_chars = list()
                sub_last_chars = list()
                divider_indices.insert(0, -1)
                for idx in range(1, len(divider_indices)):
                    sub_tokens.append(tok[divider_indices[idx - 1] + 1:divider_indices[idx]])
                    sub_first_chars.append(divider_indices[idx - 1] + 1)
                    sub_last_chars.append(divider_indices[idx])
                    sub_tokens.append(tok[divider_indices[idx]])
                    sub_first_chars.append(divider_indices[idx])
                    sub_last_chars.append(divider_indices[idx] + 1)
                # The remaining of the token
                sub_tokens.append(tok[divider_indices[-1] + 1:len(tok)])
                sub_first_chars.append(divider_indices[-1] + 1)
                sub_last_chars.append(len(tok))
                # Adjust the original token sequence
                original_first_char = first_chars[tok_idx]
                del tokens[tok_idx]
                del first_chars[tok_idx]
                del last_chars[tok_idx]
                for sub_tok_idx in range(len(sub_tokens) - 1, -1, -1):
                    tokens.insert(tok_idx, sub_tokens[sub_tok_idx])
                    first_chars.insert(tok_idx, original_first_char + sub_first_chars[sub_tok_idx])
                    last_chars.insert(tok_idx, original_first_char + sub_last_chars[sub_tok_idx])
                # Adjust sent boundaries
                self._updateSentenceBoundary(sent_bounds, tok_idx, None, len(sub_tokens) - 1)
                # Leap the original token index
                tok_idx += len(sub_tokens) - 1
            # Increase the index normally
            tok_idx += 1


# Shared resource
stanford_tokenizer = CoreNLPTokenizer2(url='http://localhost:9000')
