"""
Author: Thanh Thieu

Facilities to work with CSV format files.
"""

import csv
import os
import chardet

import entities as ents
import attributes as atts
import m_utils

ANNOTATION_EXTENSION = ".csv"
CSV_DELIMITER = '|'
CSV_FOLDER = "annotations_csv"

# ------------------------------------------------------#
# Mappings to standardize names and order of CSV fields #
# ------------------------------------------------------#

# Fixed positions of CSV fields

ENT_NAME_POS = 0  # postition of entity type name
FIRST_CHAR_POS = 1  # position of index of first character
LAST_CHAR_POS = 2  # position of index of last character
ENT_TEXT_POS = 3  # position of entity text

csv_positions = {
    ents.Mobility: {atts.MobilityType: 4, atts.Subject: 5, atts.History: 6},
    ents.Action: {atts.ICFCode: 4, atts.ActionPolarity: 5},
    ents.Assistance: {atts.AssistancePolarity: 4, atts.Source: 5},
    ents.Quantification: {atts.QuantificationType: 4},
    ents.ScoreDefinition: {}
}

# Mappings from [names in CSV file] --> [standard names of types/values in coding]
# Only map unmatched values between the 2 formats

entity_names = {"Score definition": "ScoreDefinition"}

att_values = {
    atts.History: {"1": "Past", "0": "Present", "-1": "Future",
                   "1.0": "Past", "0.0": "Present", "-1.0": "Future"},
    atts.ActionPolarity: {"1": "Able", "-1": "Unable", "0": "Unclear"},
    atts.AssistancePolarity: {"True": "Dependent", "False": "Independent"},
    atts.Source: {"Device Only": "Device", "Person Only": "Person"}
}


def decodeEntityName(name):
    """
    Given an entity name from CSV, convert it to a standard type name.
    :param name: (str)
        Entity name in CSV.
    :return: (str)
        A standard type name recognized in coding.
    """
    if name in entity_names:
        return entity_names[name]
    else:
        return name


def encodeEntityName(entity_type):
    """
    Given a standard entity type, convert it back to a CSV name.
    :param entity_type: (type)
    :return: (str)
    """
    return entity_type.__name__  # Let's use standard names in CSV output


def decodeAttributeValue(value, attribute_type):
    """
    Given an attribute value from CSV, convert it to a standard value name.
    :param name: (str)
        Attribute value in CSV.
    :param attribute_type:
        An Enum attribute type.
    :return: (str)
        A standard value name recognized in coding.
    """
    if attribute_type in att_values:
        if value in att_values[attribute_type]:
            value = att_values[attribute_type][value]
    return value


def encodeAttributeValue(value, attribute_type):
    """
    Given a standard attribute value, convert it back to a CSV value.
    :param value: (str)
        A standard attribute value in coding.
    :param attribute_type:
    :return: (str)
    """
    # if attribute_type in att_values:
    #     for csv_value, std_value in att_values[attribute_type].items():
    #         if std_value == value:
    #             return csv_value  # A match is found
    return value.name  # No match found, just use the standard value


# --------------------------#
# Read CSV annotation files #
# --------------------------#

def readCSVAnnotations(filepath):
    """
    Read entity annotations of a medical note, from a CSV file.
    :param filepath: str
        A complete path to a file that contains entity annotations.
    :return: generator(list(str))
        A generator that iterates over each line.
        Each line is converted to a list of strings, corresponding to fields of the CSV format.
    """
    # print("Reading file: {}".format(filepath)) # Debugging
    with open(filepath, mode='rb') as f:
        code_table = chardet.detect(f.read())
    with open(filepath, mode='r', encoding=code_table['encoding'], newline='') as f:
        csvreader = csv.reader(f, delimiter=CSV_DELIMITER)
        for row in csvreader:
            yield row


def readCSVAnnotationSet(folderpath):
    """
    Read annotation files from a given directory.
    :param folderpath: str
        A complete path to a directory.
    :return: generator(tuple(file_id, file_reader))
        A generator that iterates a tuple per file.
        The tuple contains (0) a file ID, and (1) a reader function over the file.
    """
    for filename in os.listdir(folderpath):
        if filename.endswith(ANNOTATION_EXTENSION):
            fileid = m_utils.extractFileID(filename)
            yield (fileid, readCSVAnnotations(os.path.join(folderpath, filename)))


# ---------------------------------------------#
# Serialize entity objects back to CSV format  #
# ---------------------------------------------#

def _csvSerializeEntity(ent):
    """
    Serialize an entity to CSV format.
    :param ent: (Derivation of Entity)
    :return: (list of str)
    """
    result = list()
    result.append(encodeEntityName(type(ent)))  # idx 0 = postition of entity type name
    result.append(str(ent.first_char_index))  # idx 1 = position of index of first character
    result.append(str(ent.last_char_index))  # idx 2 = position of index of last character
    result.append(ent.text)  # idx 3 = position of entity text

    att_types = csv_positions[type(ent)]
    for _ in range(len(att_types)):
        result.append("")
    for att_type, pos in att_types.items():
        value = ent.getAttribute(att_type)
        if value is not None:
            result[pos] = encodeAttributeValue(value, att_type)

    return result


def _csvSerializeDocument(doc):
    """
    Serialize a document to CSV format. Entities are sorted by index of the first character.
    :param doc: (Document)
    :return: (iterator of list of str)
    """
    all_ents = [ent for ent_type in ents.allEntityTypes() for ent in doc.getAnnotations(ent_type)]
    all_ents.sort(key=lambda ent: ent.first_char_index)
    for ent in all_ents:
        yield _csvSerializeEntity(ent)


def csvSerializeCorpus(C, folder_path):
    """
    Serialize a corpus to CSV files in a specified folder.

    :param C: (Corpus)
    :return: None
    """
    m_utils.safe_mkdir(folder_path)
    for doc_id in C.docIDs():
        file_path = os.path.join(folder_path, str(doc_id) + ANNOTATION_EXTENSION)
        with open(file_path, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=CSV_DELIMITER)
            for ent_ser in _csvSerializeDocument(C.getDocument(doc_id)):
                writer.writerow(ent_ser)
