"""
@Author:
    Thanh Thieu

Represent a collection of documents.
"""

import ios

import entities as ents
import attributes as atts
import m_utils
import csv_format as csvf
import integrity


class Document:
    """
    A document consists of the raw text paragraphs in a single string, plus a list of Mobility entities.
    While each Mobility contains lists of its Actions, Asssitances, and Quantifications, (<-- vertical view)
        the Document also contains a list of all Actions, Assistances, and Quantifications across all Mobilities. (<-- horizontal view)
    """

    def __init__(self, docID=None, text=None, mobilities=None, actions=None, assistances=None, quantifications=None,
                 scoredefs=None):
        """
        Create a medical document / note / record.
        :param id: (int) An integer uniquely identifies the document
        :param text: The entire raw text in a single string
        :param mobilities: A list of Mobility entities
        """
        self.m_docID = docID
        self.m_text = text
        self.m_mobilities = list() if mobilities == None else mobilities
        self.m_actions = list() if actions == None else actions
        self.m_assistances = list() if assistances == None else assistances
        self.m_quantifications = list() if quantifications == None else quantifications
        self.m_scoredefs = list() if scoredefs == None else scoredefs

    @property
    def text(self):
        return self.m_text

    @text.setter
    def text(self, value):
        self.m_text = value

    @property
    def docID(self):
        return self.m_docID

    def addEntity(self, entity_type, ent):
        """
        Add an entity into this document, based on the entity type
        :param entity_type: Type of the entity.
        :param ent: The entity itself.
        :return: None
        """
        if entity_type == ents.Mobility:
            self.m_mobilities.append(ent)
        elif entity_type == ents.Action:
            self.m_actions.append(ent)
        elif entity_type == ents.Assistance:
            self.m_assistances.append(ent)
        elif entity_type == ents.Quantification:
            self.m_quantifications.append(ent)
        elif entity_type == ents.ScoreDefinition:
            self.m_scoredefs.append(ent)
        else:
            raise TypeError("Cannot recognize entity type: " + entity_type.__name__)

    def getAnnotations(self, entity_type):
        """
        Get the list of annotations of a specific entity type.
        :param entity_type: (subclass of Entity)
        :return: (list of entities)
        """
        if entity_type == ents.Mobility:
            return self.m_mobilities
        elif entity_type == ents.Action:
            return self.m_actions
        elif entity_type == ents.Assistance:
            return self.m_assistances
        elif entity_type == ents.Quantification:
            return self.m_quantifications
        elif entity_type == ents.ScoreDefinition:
            return self.m_scoredefs
        else:
            raise TypeError("Cannot recognize entity type: " + entity_type.__name__)


class Corpus:
    """
    A corpus is a collection of documents.
    """

    def __init__(self, rawnote_folders=None, annotation_folders=None, corpus_ID=None):
        """
        Create a corpus as a mapping from document IDs to actual Documents.

        :param rawnote_folders: (list of str) A list of complete paths to folders that contain raw note files.
        :param annotation_folders: (list of str) A list of complete paths to folders that contain annotation files.
        :param corpus_ID: (int) Index of this parallel corpus. If None then singular corpus.
        """
        self.m_docs = dict()  # mapping from docID to the Document
        self.extendCorpus(rawnote_folders, annotation_folders, corpus_ID)

    def extendCorpus(self, rawnote_folders=None, annotation_folders=None, corpus_ID=None):
        """
        Load additional data into an existing corpus. This allow for a different parallel corpus ID.

        :param rawnote_folders: (list of str) A list of complete paths to folders that contain raw note files.
        :param annotation_folders: (list of str) A list of complete paths to folders that contain annotation files.
        :param corpus_ID: (int) Index of this parallel corpus. If None then singular corpus.
        :return:
        """
        # Read in raw text
        if rawnote_folders:
            for folder in rawnote_folders:
                rawdata = ios.readRawNotes(folder)
                for record in rawdata.items():
                    docID = record[0] if corpus_ID is None else m_utils.parallelDocumentID(record[0], corpus_ID)
                    text = record[1]
                    if docID in self.m_docs:
                        self.m_docs[docID].text = text
                    else:
                        newdoc = Document(docID, text)
                        self.m_docs[docID] = newdoc

        # Read in annotation
        if annotation_folders:
            for folder in annotation_folders:
                for (file_id, fileReader) in csvf.readCSVAnnotationSet(folder):
                    if corpus_ID is not None:
                        file_id = m_utils.parallelDocumentID(file_id, corpus_ID)
                    # Indentify the document
                    if file_id in self.m_docs:
                        current_doc = self.m_docs[file_id]
                    else:
                        current_doc = Document(file_id)
                        self.m_docs[file_id] = current_doc

                    # Read entities into the document
                    for anno in fileReader:
                        # Read textual span
                        firstchar_idx = int(anno[csvf.FIRST_CHAR_POS])
                        lastchar_idx = int(anno[csvf.LAST_CHAR_POS])
                        text = anno[csvf.ENT_TEXT_POS]
                        # Integrity fix of the entity textual span
                        text, firstchar_idx, lastchar_idx = \
                            integrity.fixEntityLeadingTrailingSpaces(text, firstchar_idx, lastchar_idx)
                        # Create the span
                        span = atts.TextSpan(firstchar_idx, lastchar_idx, text)

                        # Read other attributes
                        entity_type = m_utils.lookupEntityType(csvf.decodeEntityName(anno[csvf.ENT_NAME_POS]))
                        att_mapping = dict()
                        for att_type in csvf.csv_positions[entity_type]:
                            value = m_utils.lookupAttributeValue(
                                csvf.decodeAttributeValue(anno[csvf.csv_positions[entity_type][att_type]], att_type),
                                att_type)
                            att_mapping[att_type] = value

                        # Create the entity
                        entity = entity_type(span, att_mapping=att_mapping)

                        # Record the entity
                        current_doc.addEntity(entity_type, entity)

                    # Build vertical view of Mobility towards its sub-entities
                    DocumentFactory.collectMobilityVerticalView(current_doc)
                    # Check integrity of character indices of the entities
                    integrity.checkEntityTextspan(file_id, current_doc)

    def addDocument(self, doc_id, doc):
        """
        Add an additional document to this corpus.
        :param doc_id:
        :param doc:
        :return:
        """
        self.m_docs[doc_id] = doc

    def removeDocuments(self, doc_ids):
        """
        Remove some documents from this corpus.
        This is useful when two corpora don't exactly match document IDs.

        :param doc_ids: A list of document IDs to be removed from this corpus.
        :return:
        """
        for doc_id in doc_ids:
            del self.m_docs[doc_id]

    def getDocument(self, docID):
        """
        Get a document by ID.
        :param docID: 
        :return: 
        """
        return self.m_docs[docID]

    def docIDs(self):
        """
        Return a collection of IDs of documents presented in this corpus.
        :return: 
        """
        return set(self.m_docs.keys())


class DocumentFactory:
    """
    Provide facilities to structure a document from input not readily in the format expected by the Document initializer.
    """

    @classmethod
    def collectMobilityVerticalView(cls, doc):
        """
        Build vertical view of Mobility entities towards its sub-entities.
        :param doc:
            A Document.
        :return:
            The same document with its Mobility entities updated to contain engulfed sub-entities.
        """
        # Collect sub-entities for Mobility entities
        for sub_entity_type in ents.subEntityTypesOfMobility():
            for ent in doc.getAnnotations(sub_entity_type):
                for mob in doc.getAnnotations(ents.Mobility):
                    if mob.m_textspan.engulf(ent.m_textspan):
                        mob.addSubEntity(sub_entity_type, ent)

        # Sort sub-entities by first character indices
        for mob in doc.getAnnotations(ents.Mobility):
            for sub_entity_type in ents.subEntityTypesOfMobility():
                mob.getSubEntities(sub_entity_type).sort(key=lambda x: x.first_char_index)

    @classmethod
    def createDocument(cls, docID, text, coalesced_entities):
        """
        :param text:
            Original text of the entire document.
        :param coalesced_entities:
            A sequence of tuples (first char index, last char index, entity type).
        :return:
        """
        doc = Document(docID, text)
        for first_char_idx, last_char_idx, entity_type in coalesced_entities:
            span = atts.TextSpan(first_char_idx, last_char_idx, text[first_char_idx:last_char_idx])
            ent = entity_type(span)
            doc.addEntity(entity_type, ent)
        cls.collectMobilityVerticalView(doc)
        return doc
