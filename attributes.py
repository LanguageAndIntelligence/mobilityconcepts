"""
@Author: 
    Thanh Thieu

Define all attributes of the framework.
"""

import tokenizer


class TextSpan:
    """
    A text span keeps track of the character indices at the beginning and the end of the span.
    It optionally has the actual text.
    """

    def __init__(self, first_char_index, last_char_index, text=None):
        """
        Create a text span with first character index (inclusive) and last character index (exclusive)
        w.r.t. the document containing this span.
        :param first_char_index: int
            First character index, inclusive
        :param last_char_index: int
            Last character index, exclusive
        :param text: str
            (Optional) The actual text of the span.
        """
        self.m_firstchar_index = first_char_index
        self.m_lastchar_index = last_char_index
        self.m_text = text
        # [0] contains tokens, [1] contains sentence boundaries
        self.m_tokens = tokenizer.stanford_tokenizer.tokenize(text)[0] if text is not None else None

    def numTokens(self):
        """
        Get number of tokens of this text span.
        :return: 
        """
        return len(self.m_tokens)

    def overlap(self, other):
        """
        Check if this textual span overlap in any capacity with another textual span.
        :param other: 
        :return: (boolean)
            True if overlapping, False otherwise.
        """
        return (self.m_firstchar_index <= other.m_firstchar_index <= self.m_lastchar_index) \
               or (
                       self.m_firstchar_index <= other.m_lastchar_index <= self.m_lastchar_index) \
               or (
                       other.m_firstchar_index <= self.m_firstchar_index <= other.m_lastchar_index) \
               or (
                       other.m_firstchar_index <= self.m_lastchar_index <= other.m_lastchar_index)

    def engulf(self, other):
        """
        Check if this textual span completely cover another textual span.
        :param other: 
        :return: (boolean)
            True if this textual span over shadow the other textual span, False otherwise.
        """
        return (self.m_firstchar_index <= other.m_firstchar_index and self.m_lastchar_index >= other.m_lastchar_index)

    def exact(self, other, compare_string_content=False):
        """
        Check if this textual span matches exactly to another textual span by first/last character indices.
        :param other:
        :param compare_string_content:
            If True then also compare string content of the two entities.
            This is helpful when the two documents are shifted by a few characters but the contents are the same.
        :return:
        """
        index_matched = self.m_firstchar_index == other.m_firstchar_index and self.m_lastchar_index == other.m_lastchar_index
        if index_matched:
            return True
        elif compare_string_content:
            return self.overlap(other) and self.m_text == other.m_text
        else:
            return False

    def __eq__(self, other):
        """
        Two textspans are the same if indices of their first/last characters are the same.
        :param other:
        :return:
        """
        return self.exact(other, False)


# ---------------------
# Contextual Attributes
# ---------------------

from enum import Enum


class MobilityType(Enum):
    """
    Values for Type attribute of Mobility Entity
    """
    Objective = 0
    Subjective = 1


class Subject(Enum):
    """
    Values for Subject attribute of Mobility Entity
    """
    Patient = 0
    Other = 1


class History(Enum):
    """
    Values for History attribute of Mobility Entity
    """
    Past = 1
    Present = 0
    Future = -1


class ICFCode(Enum):
    """
    Values of Subdomain Code attribute of Action Entity
    """
    d410 = 0
    d415 = 1
    d420 = 2
    d430 = 3
    d435 = 4
    d440 = 5
    d445 = 6
    d450 = 7
    d455 = 8
    d460 = 9
    d470 = 10
    d475 = 11
    d480 = 12


class ActionPolarity(Enum):
    """
    Values of Polarity attribute of Action Entity
    """
    Able = 1
    Unable = -1
    Unclear = 0


class AssistancePolarity(Enum):
    """
    Values of Polarity attribute of Assistance Entity
    """
    Dependent = 1
    Independent = -1
    Unclear = 0


class Source(Enum):
    """
    Values of Source attribute of Assistance Entity
    """
    Device = 0
    Person = 1
    Other = 2


class QuantificationType(Enum):
    """
    Values of Type attribute of Quantification Entity
    """
    Distance = 0
    Time = 1
    Scale = 2
    Frequency = 3
