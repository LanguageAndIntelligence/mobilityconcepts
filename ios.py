"""
@Author:
    Thanh Thieu

Handle input / output operations.
"""

import openpyxl
import os

import m_utils
import config

# --------------------
# Directory structure
# --------------------

DATA_DIR = config.getConfig().DATA_DIR
LIB_DIR = config.getConfig().LIB_DIR
RAW_NOTE_EXTENSION = ".txt"


# ---------------------------------
# Read raw text medical note files
# ---------------------------------

def readRawNote(filepath):
    """
    Read raw text from a medical note.
    :param filepath: str
        A complete path to a file that contains a single note.
    :return: str
        Content of the note.
    """
    with open(filepath, mode='r', encoding=m_utils.get_file_encoding(filepath)) as f:
        return f.read()


def readRawNotes(folderpath):
    """
    Read all raw medical notes from a given directory.
    :param folderpath: str
        A complete path to a directory.
    :return: dict
        A mapping from file name (as ID) to raw text.
    """
    rawdata = {}
    for filename in os.listdir(folderpath):
        if filename.endswith(RAW_NOTE_EXTENSION):
            text = readRawNote(os.path.join(folderpath, filename))
            fileid = m_utils.extractFileID(filename)
            rawdata[fileid] = text
    return rawdata


# ------------------------------------------------------
# Extract original BTRIS notes from Excel to plain text
# ------------------------------------------------------

# Structure of the Excel file that contains BTRIS notes
NOTE_ID_COLUMN = 1  # 1st column
NOTE_TYPE_COLUMN = 5  # 5th column
NOTE_CONTENT_COUMN = 6  # 6th column


def extractNotesFromExcel(inpfile, subdir):
    '''
    Read every note from single Excel rows and convert them to plain text files.
    :param inpfile: Name of the Excel file to read.
    :param subdir: The relative sub-folder to read/write notes.
    :return: Save each note to a separate plain text file.
    '''

    wb = openpyxl.load_workbook(os.path.join(DATA_DIR, subdir, inpfile))
    raw_dir = os.path.join(DATA_DIR, subdir, os.path.splitext(inpfile)[0])
    if not os.path.exists(raw_dir):
        os.mkdir(raw_dir)
    for sheetname in wb.get_sheet_names():
        sheet_dir = os.path.join(raw_dir, sheetname)
        if not os.path.exists(sheet_dir):
            os.mkdir(sheet_dir)
        sheet = wb.get_sheet_by_name(sheetname)
        for rowidx in range(1, sheet.max_row + 1):
            note_id = sheet.cell(row=rowidx, column=NOTE_ID_COLUMN).value
            if note_id != None:
                # full name of the note includes its ID and its document type
                # note_name = str(note_id) + " - " + sheet.cell(row = rowidx, column = NOTE_TYPE_COLUMN).value
                note_name = str(note_id)
                with open(os.path.join(sheet_dir, note_name + RAW_NOTE_EXTENSION), "w") as f:
                    f.write(sheet.cell(row=rowidx, column=NOTE_CONTENT_COUMN).value)


#########################
# Standalone Executable #
#########################

def unit_test():
    extractNotesFromExcel("PT_Discharge_Note_Raw.xlsx", "raw_notes")
    # readRawNotes(os.path.join(DATA_DIR, RAW_NOTE_SUBDIR))


if __name__ == "__main__":
    unit_test()
