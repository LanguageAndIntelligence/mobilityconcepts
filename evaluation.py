"""
@Author
    Thanh Thieu
    
Compute performance measures.
"""

# jellyfish (https://pypi.python.org/pypi/jellyfish): a library for doing approximate and phonetic matching of strings

import csv
import itertools
import os
import pickle
from collections import defaultdict

import numpy as np
import pandas as pd
from nltk.metrics import accuracy as bio_accuracy

import attributes as atts
import entities as ents
import tokenizer


def _isOverlapping(ent1, ent2):
    """
    Check if the 2 entities overlap.
    :param ent1: 
    :param ent2: 
    :return: 
    """
    return ent1.m_textspan.overlap(ent2.m_textspan)


def _isExact(ent1, ent2, compare_string_content=False):
    """
    Check if 2 entities' text spans match exactly by first/last character indices.

    :param ent1:
    :param ent2:
    :param compare_string_content: If True then also compare string content of the two entities.
    :return:
    """
    return ent1.m_textspan.exact(ent2.m_textspan, compare_string_content)


def _align(anno1, anno2, fashion):
    """
    Align entities of the same type between two annotation versions of the same document.

    :param anno1: (List of entities) 
        An annotation of a document.
    :param anno2: (List of entities)
        Another annotation of the same document.
    :param fashion: (str)
        The alignment condition. Valid values are: "partial", "exact".
    :return: (List of tuples of entities)
    """
    alignments = list()
    ent2_used = [False] * len(anno2)
    for ent1_idx in range(len(anno1)):
        ent1 = anno1[ent1_idx]
        ent1_used = False
        for ent2_idx in range(len(anno2)):
            ent2 = anno2[ent2_idx]

            if fashion == "partial":
                align_ok = _isOverlapping(ent1, ent2)
            elif fashion == "exact":
                align_ok = _isExact(ent1, ent2, False)  # whether to compare string content will make sizable impact
            else:
                raise ValueError("Unidentified value of 'fashion' parameter.")

            if align_ok:
                alignments.append((ent1, ent2))
                ent1_used = True
                ent2_used[ent2_idx] = True
        if not ent1_used:
            alignments.append((ent1, None))

    for ent2_idx in range(len(anno2)):
        if not ent2_used[ent2_idx]:
            alignments.append((None, anno2[ent2_idx]))

    return alignments


def _countOverlappingTokens(ent1, ent2):
    """
    Count number of overlapping tokens between the 2 entities.
    :param ent1: 
    :param ent2: 
    :return: 
    """
    indices = list()
    indices.append(ent1.m_textspan.m_firstchar_index)
    indices.append(ent1.m_textspan.m_lastchar_index)
    indices.append(ent2.m_textspan.m_firstchar_index)
    indices.append(ent2.m_textspan.m_lastchar_index)
    indices.sort()
    first = indices[1] - ent1.m_textspan.m_firstchar_index
    last = indices[2] - ent1.m_textspan.m_firstchar_index
    overlap = ent1.m_textspan.m_text[first:last]
    overlap = tokenizer.stanford_tokenizer.tokenize(overlap)[0]
    return len(overlap)


def _computePerformance(TP, FP, FN):
    """
    Compute and return performance metrics given raw performance counts.
    :param TP: number of true positives
    :param FP: number of false positives
    :param FN: number of false negatives
    :return: (dict)
        A dictionary mapping each measure to its value.
    """
    precision = float(TP) / (TP + FP) if TP + FP > 0 else None
    recall = float(TP) / (TP + FN) if TP + FN > 0 else None
    F1 = 2 * precision * recall / (precision + recall) \
        if precision is not None and recall is not None and precision + recall > 0 else None
    return {"TP": TP, "FP": FP, "FN": FN, "precision": precision, "recall": recall, "F1": F1}


def evaluateTextSpan(alignments, alignment_fashion):
    """
    Compute textual span agreement of a list of entity alignments. 
    :param alignments: (List of pairs of entities) 
        Within each alignment pair, the first entity is response, and the second entity is gold standard.
    :param alignment_fashion: (str)
        How 2 entities are matched. Valid values are: "partial", "exact".
    :return: 
    """
    resp_idx = 0
    gold_idx = 1
    TP = 0.  # true positives
    FP = 0.  # false positives
    FN = 0.  # false negatives
    for align_pair in alignments:
        resp = align_pair[resp_idx]
        gold = align_pair[gold_idx]
        if resp is None:
            FN += 1
        elif gold is None:
            FP += 1
        else:
            if alignment_fashion == "exact":
                TP += 1
            else:  # alignment_fashion == "partial":
                if _isExact(resp, gold, False):  # fully matched
                    TP += 1
                else:  # overlapping
                    TP += 0.5
                    FP += 0.5
                    FN += 0.5
                # # Old way under reviewer criticism
                # common = _countOverlappingTokens(resp, gold)
                # TP += 2 * float(common) / (resp.m_textspan.numTokens() + gold.m_textspan.numTokens())
                # FP += float(resp.m_textspan.numTokens() - common) / resp.m_textspan.numTokens()
                # FN += float(gold.m_textspan.numTokens() - common) / gold.m_textspan.numTokens()

    return _computePerformance(TP, FP, FN)


def _observedAgreement(alignments, attribute_type):
    """
    Compute the portion of the instances on which the annotators agree.
    :param alignments: 
    :param attribute_type: 
    :return: 
    """
    count_agree = 0  # count number of agreement
    count_pairs = 0  # actual pairs that align between the 2 annotators
    for align_pair in alignments:
        # Only count if both annotators capture the same entity
        if align_pair[0] is not None and align_pair[1] is not None:
            count_pairs += 1
            val0 = align_pair[0].getAttribute(attribute_type)
            val1 = align_pair[1].getAttribute(attribute_type)
            if val0 == val1:
                count_agree += 1

    if count_pairs > 0:
        return float(count_agree) / count_pairs
    else:
        return None


def samplePrevalence(alignments, attribute_type):
    """
    Compute sample prevalence w.r.t. each value of a attribute.
    Note: This is not the underlying sample prevalence since we don't know the true data. This is actually the expected observed prevalence.
    To avoid naming confusion with observed agreement, we call this observed sample prevalence.
    :param alignments:
    :param attribute_type:
    :return:
    """
    # Build an observed contingency table for this attribute
    none_name = None.__str__()
    values = list(attribute_type.__members__) + [none_name]  # None represents a blank value
    contingency_table = pd.DataFrame(data=np.zeros((len(values), len(values))), index=values, columns=values,
                                     dtype=np.int32)
    for (ent1, ent2) in alignments:
        if ent1 is not None and ent2 is not None:  # Both annotators capture the same entity
            val1 = ent1.getAttribute(attribute_type).name if ent1.getAttribute(attribute_type) is not None \
                else none_name
            val2 = ent2.getAttribute(attribute_type).name if ent2.getAttribute(attribute_type) is not None \
                else none_name
            contingency_table.at[val1, val2] += 1

    # Compute prevalence of each attribute value
    col_sums = contingency_table.sum(axis=0) / 2
    row_sums = contingency_table.sum(axis=1) / 2
    total = contingency_table.sum().sum()
    prevalence = dict()
    for val in values:
        p = (col_sums.at[val] + row_sums.at[val]) / total
        prevalence[val] = p

    return prevalence


def _computeAttributeFreq(alignments, attribute_type):
    """
    Count frequency of each attribute value, within the given attribute type, w.r.t. 2 annotators.
    :param alignments: 
    :param attribute_type: 
    :return: Two numpy arrays, and the actual number of aligned pairs
    """
    # Count frequency of each attribute value
    num_align = 0  # actual aligned pairs (i.e. not None)
    count0 = {None: 0}  # count attribute values for annotator 0
    for att_val in attribute_type:
        count0[att_val] = 0
    count1 = {None: 0}  # count attribute values for annotator 1
    for att_val in attribute_type:
        count1[att_val] = 0

    for align_pair in alignments:
        if align_pair[0] is not None and align_pair[1] is not None:  # Only count actual alignment
            num_align += 1
            val0 = align_pair[0].getAttribute(attribute_type)
            count0[val0] += 1
            val1 = align_pair[1].getAttribute(attribute_type)
            count1[val1] += 1

    # Align the frequency to numpy arrays
    vec0 = np.zeros(len(attribute_type) + 1)  # 1 additional count for None value, i.e. blank value
    vec1 = np.zeros(len(attribute_type) + 1)
    idx = -1
    for att_val in list(attribute_type) + [None]:
        idx += 1
        vec0[idx] = count0[att_val]
        vec1[idx] = count1[att_val]

    return vec0, vec1, num_align


def _chanceAgreementIndivDist(alignments, attribute_type):
    """
    Compute chance agreement between 2 annotators, based on the individual distribution of each annotator.
    :param alignments: 
    :param attribute_type: 
    :return: 
    """
    vec0, vec1, num_align = _computeAttributeFreq(alignments, attribute_type)

    # Compute change agreement
    vec0 /= num_align
    vec1 /= num_align
    agree = vec0 * vec1
    return np.sum(agree)


def _chanceAgreementJointDist(alignments, attribute_type):
    """
    Compute chance agreement between 2 annotators, based on the joint distribution of the annotators.
    :param alignments: 
    :param attribute_type: 
    :return: 
    """
    vec0, vec1, num_align = _computeAttributeFreq(alignments, attribute_type)

    # Compute change agreement
    total = vec0 + vec1
    total /= 2 * num_align
    total = np.square(total)
    total = np.sum(total)
    return total


def kappaCohen(alignments, attribute_type):
    """
    Compute Cohen's Kappa for 2 annotators w.r.t. a specific attribute.
    :param alignments: 
    :param attribute_type:
    :return: 
    """
    Ao = _observedAgreement(alignments, attribute_type)
    Ae = _chanceAgreementIndivDist(alignments, attribute_type)
    if Ao is not None and Ae is not None:
        return (Ao - Ae) / (1 - Ae), Ao, Ae  # also return observed & chance for checkup
    else:
        return None, Ao, Ae


def kappaSiegelCastellan(alignments, attribute_type):
    """
    Compute Siegel & Castellan’s Kappa for 2 annotators w.r.t. a specific attribute.
    :param alignments: 
    :param attribute_type: 
    :return: 
    """
    Ao = _observedAgreement(alignments, attribute_type)
    Ae = _chanceAgreementJointDist(alignments, attribute_type)
    return (Ao - Ae) / (1 - Ae), Ao, Ae  # also return observed & chance for checkup


def _commonDocIDs(anno1, anno2):
    """
    Find common documnet IDs present in both corpuses.

    :param anno1: (Corpus) Annotation result of annotator 1.
    :param anno2: (Corpus) Annotation result of annotator 2, on the same dataset.
    :return: (set) Common doc IDs between the 2 corpuses.
    """
    ids1 = anno1.docIDs()
    ids2 = anno2.docIDs()
    print("Corpus 1 has {} documents".format(len(ids1)))
    print("Corpus 2 has {} documents".format(len(ids2)))

    diff1 = ids1.difference(ids2)
    if len(diff1) > 0:
        print("{} documents in corpus 1 but not in corpus 2:".format(len(diff1)))
        print(diff1)
    diff2 = ids2.difference(ids1)
    if len(diff2) > 0:
        print("{} documents in corpus 2 but not in corpus 1:".format(len(diff2)))
        print(diff2)

    common_ids = ids1.intersection(ids2)
    print("{} common documents present in both corpuses".format(len(common_ids)))

    return common_ids


def _microAggregation(anno1, anno2, common_ids, entity_type, alignment_fashion):
    """
    Align and aggregate annotations of 2 annotators, w.r.t. a given entity type.

    :param anno1: (Corpus) Annotation result of annotator 1.
    :param anno2: (Corpus) Annotation result of annotator 2, on the same dataset.
    :param common_ids: (set) Common doc IDs between the 2 corpuses.
    :param entity_type:
    :param alignment_fashion: (str) How 2 entities are matched. Valid values are: "partial", "exact".
    :return: (List of tuple of entities) 
        A list of aligned tuples of the given entity type, aggregated across all documents.
    """
    alignments = list()
    for docID in common_ids:
        doc_algm = _align(anno1.getDocument(docID).getAnnotations(entity_type),
                          anno2.getDocument(docID).getAnnotations(entity_type),
                          alignment_fashion)
        alignments.extend(doc_algm)

    return alignments


def evaluateAgreement(response_corpus, gold_standard_corpus, alignment_fashion):
    """
    Compute agreement between 2 annotation results.
    Agreement comprises of entities' text spans and attributes' values.
    :param response_corpus: (Corpus) 
        Response annotation.
    :param gold_standard_corpus: (Corpus)
        Gold standard annotation.
    :param alignment_fashion: (str)
        How 2 entities are matched. Valid values are: "partial", "exact".
    :return: (dict) 
        A comprehensive dictionary containing agreement evaluation across all entity types and all attribute types.
    """
    agreement = defaultdict(lambda: defaultdict(dict))
    common_ids = _commonDocIDs(response_corpus, gold_standard_corpus)
    for ent_type in ents.allEntityTypes():
        alignments = _microAggregation(response_corpus, gold_standard_corpus, common_ids, ent_type, alignment_fashion)
        if alignments:  # There must be something in the alignments, otherwise just ignore this entity type
            perf = evaluateTextSpan(alignments, alignment_fashion)
            agreement[ent_type][atts.TextSpan] = perf
            for att_type in ent_type.attributeTypes():
                kappa, Ao, Ae = kappaCohen(alignments, att_type)
                agreement[ent_type][att_type]["CohenKappa"] = kappa
                agreement[ent_type][att_type]["Cohen_Ao"] = Ao
                agreement[ent_type][att_type]["Cohen_Ae"] = Ae
                att_value_prevalence = samplePrevalence(alignments, att_type)
                agreement[ent_type][att_type]["Prevalence"] = \
                    "; ".join(["{}={:.3f}".format(key, val) for (key, val) in att_value_prevalence.items()])
                # agree_joint, Ao_joint, Ae_joint = kappaSiegelCastellan(alignments, att_type)
                # agreement[ent_type][att_type]["SiegelCastellanKappa"] = agree_joint
                # agreement[ent_type][att_type]["SiegelCastellan_Ao"] = Ao_joint
                # agreement[ent_type][att_type]["SiegelCastellan_Ae"] = Ae_joint
    return agreement


def writeCSV(perf, file_path):
    """
    Write evaluation results to a CSV file.
    :param perf: (dict) 
        A nested (3 levels of keys) dictionary containing all evaluation data.
    :param file_path: (str) 
        Path to a CSV file to be written.
    :return: None
    """
    with open(file_path, 'w', newline='') as f:
        writer = csv.writer(f)
        for level1 in perf:
            for level2 in perf[level1]:
                for level3 in sorted(perf[level1][level2].keys()):
                    writer.writerow([level1, level2, level3, perf[level1][level2][level3]])
                writer.writerow([])  # empty line
            writer.writerow([])  # empty line


def evaluateBIOSequences(golden_seq, predicted_seq):
    """
    Compute accuracy/precision based on B/I/O tag sequences.
    :param golden_seq: List of list of B/I/O tags. This is the golden standard sequences.
    :param predicted_seq: List of list of B/I/O tags. This is the predicted sequences.
    :return: Accuracy/precision of how well the prediction matches the golden standard.
    """
    return bio_accuracy(list(itertools.chain.from_iterable(golden_seq)),
                        list(itertools.chain.from_iterable(predicted_seq)))


def combineEval2CSV(pickle_files, csv_file):
    """
    Write multiple evaluation results into a single CSV file.

    :param pickle_files: A list of pickle files containing evaluation results.
    :param csv_file: Output CSV file.
    :return: None
    """
    # Read evaluation results
    perfs = list()
    for file_path in pickle_files:
        data = pickle.load(open(file_path, "rb"))
        perfs.append(data)

    # Write evaluation info
    with open(csv_file, 'w', newline='') as f:
        writer = csv.writer(f)
        # Write header
        file_names = [os.path.splitext(os.path.basename(f))[0] for f in pickle_files]
        writer.writerow([""] * 3 + file_names)
        # Write measures
        for level1 in perfs[0]:
            for level2 in perfs[0][level1]:
                for level3 in sorted(perfs[0][level1][level2].keys()):
                    writer.writerow([level1, level2, level3] +
                                    [perf[level1][level2][level3] for perf in perfs])
                writer.writerow([])  # empty line
            writer.writerow([])  # empty line
