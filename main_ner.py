"""
Author: Thanh Thieu

Main entry point of the program.
"""

import argparse
import os
import pickle
import sys
from timeit import default_timer as timer

import config
import corpus
import cross_validation
import csv_format
import entities
import evaluation
import ios
import m_utils
import tags
import tokenizer as tokenizing
from taggers import crf
from taggers import ensembler
from taggers import lstm_neuroner

ENTITIES_TO_TAG = {
    'all_ents': [entities.Mobility, entities.Action, entities.Assistance, entities.Quantification,
                 entities.ScoreDefinition],
    'mob': [entities.Mobility],
    'act': [entities.Action],
    'asst': [entities.Assistance],
    'quant': [entities.Quantification],
    'scdf': [entities.ScoreDefinition],
    'mob_act': [entities.Mobility, entities.Action],
    'mob_asst': [entities.Mobility, entities.Assistance],
    'mob_quant': [entities.Mobility, entities.Quantification],
    'mob_scdf': [entities.Mobility, entities.ScoreDefinition],
    'act_asst_quant': [entities.Action, entities.Assistance, entities.Quantification]
}

# Cache the dataset, to avoid loading multiple times
singular_dataset = None
parallel_dataset = None
gold_dataset = None


def _loadGoldStandardData(load_raw_text):
    """
    Load the Gold Standard datasets.

    :param load_raw_text: True/False
    :return: C
    """
    print("\nLoad GOLD dataset")
    global gold_dataset

    rawnote_basedir = os.path.join(ios.DATA_DIR, "Annotation_team",
                                   "GATE annotations with updated guideline - old redaction",
                                   "txt output", "GSC_400_organized")
    rawnote_paths = [
        os.path.join(rawnote_basedir, "PT_initial_assessment_200"),
        os.path.join(rawnote_basedir, "PT_reassessment_150"),
        os.path.join(rawnote_basedir, "PT_Stanford_50")]
    if not load_raw_text:
        rawnote_paths = None

    annotation_basedir = os.path.join(ios.DATA_DIR, "Annotation_team",
                                      "GATE annotations with updated guideline - old redaction",
                                      "CSV output")

    if not gold_dataset:
        gold_dataset = corpus.Corpus(rawnote_folders=rawnote_paths,
                                     annotation_folders=[os.path.join(annotation_basedir, "GSC_400_all")])

    print("Gold Standard Dataset loaded\n")
    return gold_dataset


def _loadPostData(load_raw_text, load_parallel_corpuses):
    """
    Load the Post datasets.

    :param load_raw_text: True/False
    :param load_parallel_corpuses: True/False
    :return:
        (C1, C2) if loading independent corpuses.
         or (Corpus, int) The loaded corpus, together with number of parallel corpuses, if loading parallel corpuses.
    """
    print("\nLoading POST datasets...")
    global parallel_dataset
    global singular_dataset

    annotation_team_basedir = os.path.join(ios.DATA_DIR, "Annotation_team",
                                           "GATE annotations with updated guideline - new redaction")

    rawnote_basedir = os.path.join(annotation_team_basedir, "original")

    rawnote_paths = [
        os.path.join(rawnote_basedir, "PT_initial_assessment_200"),
        os.path.join(rawnote_basedir, "PT_reassessment_150"),
        os.path.join(rawnote_basedir, "PT_Stanford_50")]
    if not load_raw_text:
        rawnote_paths = None

    annotation_basedir = os.path.join(annotation_team_basedir, "CSV output")

    annotation_paths_Jona = [
        os.path.join(annotation_basedir, "GATE_Jona_Updated_guideline_PT_initial_assessment_200_post"),
        os.path.join(annotation_basedir, "GATE_Jona_Updated_guideline_PT_reassessment_150_post"),
        os.path.join(annotation_basedir, "GATE_Jona_Updated_guideline_Stanford_50_post")]
    annotation_paths_PeiShu = [
        os.path.join(annotation_basedir, "GATE_Pei-Shu_Updated_guideline_PT_initial_assessment_200_post"),
        os.path.join(annotation_basedir, "GATE_Pei-Shu_Updated_guideline_PT_reassessment_150_post"),
        os.path.join(annotation_basedir, "GATE_Pei-Shu_Updated_guideline_Stanford_50_post")]

    if load_parallel_corpuses:
        if not parallel_dataset:
            C = corpus.Corpus()
            C.extendCorpus(rawnote_folders=rawnote_paths, annotation_folders=annotation_paths_Jona, corpus_ID=1)
            C.extendCorpus(rawnote_folders=rawnote_paths, annotation_folders=annotation_paths_PeiShu, corpus_ID=2)
            parallel_dataset = (C, 2)
    else:  # singular corpuses
        if not singular_dataset:
            C1 = corpus.Corpus(rawnote_folders=rawnote_paths, annotation_folders=annotation_paths_Jona)
            C2 = corpus.Corpus(rawnote_folders=rawnote_paths, annotation_folders=annotation_paths_PeiShu)
            singular_dataset = (C1, C2)

    print("Post Dataset loaded\n")

    if load_parallel_corpuses:
        return parallel_dataset
    else:
        return singular_dataset


def _loadMachineRecognitionData(csv_path):
    """
    Load tagging data generated by machine algorithms.

    :param csv_path: (str) Path to a directory with CSV files.
    :return:
    """
    C = corpus.Corpus(annotation_folders=[csv_path])
    return C


def _split_traintest(C, fold, ignore_empty, num_parallel_corpuses, dataset_name=""):
    """
    Split the corpus into training & testing data

    :param C: A Corpus.
    :param fold: Fold number of cross-validation.
    :param ignore_empty: (bool) If True then ignore documents that don't contain any functional info.
    :param num_parallel_corpuses: (int) Number of parallel corpuses. If None then a singular corpus.
    :param dataset_name: (str) Name of the dataset. To be used to form deeper directory structure.
    :return:
    """
    cross_validation.setDatasetName(dataset_name)
    if cross_validation.countFolds() <= 0:
        if ignore_empty:
            doc_ids = [doc_id for doc_id in C.docIDs() if
                       len(C.getDocument(doc_id).getAnnotations(entities.Mobility)) > 0]
        else:
            doc_ids = list(C.docIDs())
        print("Generate {} folds cross-validation data from {} documents".format(cross_validation.MAX_NUM_FOLDS,
                                                                                 len(doc_ids)))
        cross_validation.splitData(doc_ids, cross_validation.MAX_NUM_FOLDS, num_parallel_corpuses)

    print("Load fold {0} data".format(fold))
    train_doc_ids, val_doc_ids, test_doc_ids = cross_validation.getFoldData(fold)
    return train_doc_ids, val_doc_ids, test_doc_ids


def _prepare_data(C, doc_ids, entities_to_tag, tokenizer, text_preprocessor, split_sents):
    """
    Convert the data to a format ready for training & testing by a classifier.

    :param C: A corpus, potentially paralleled.
    :param doc_ids:
    :param entities_to_tag:
    :param tokenizer:
    :param split_sents: If True then segment each doc into sentences.
        Also append sent index to each doc ID.
        Only split (token,tag) pairs and (tokens) lists.
    :return: pairs_list, tokens_list, tags_list, spans_list, sub_corpus, sent_ids, doc_bounds
    """
    pairs_list = list()
    tokens_list = list()
    tags_list = list()
    spans_list = list()
    sub_corpus = corpus.Corpus()
    sent_ids = list() if split_sents else None  # concat doc ID with sent index
    doc_bounds = list() if split_sents else None  # how many sentences in each document; structured similar to sent bounds

    for doc_idx in range(len(doc_ids)):  # To preserve order of documents
        doc_id = doc_ids[doc_idx]
        doc = C.getDocument(doc_id)
        pairs_, tags_, tokens_, spans_, sent_bounds_ = \
            tags.pairTokensBIOTags(doc, entities_to_tag, tokenizer, text_preprocessor)

        if split_sents:
            doc_bounds.append(len(sent_bounds_) + (0 if len(doc_bounds) == 0 else doc_bounds[-1]))
            for sent_idx in range(len(sent_bounds_)):
                start = 0 if sent_idx == 0 else sent_bounds_[sent_idx - 1]
                end = sent_bounds_[sent_idx]
                pairs_list.append(pairs_[start:end])
                tokens_list.append(tokens_[start:end])
                sent_ids.append(m_utils.sentenceID(doc_id, sent_idx))
        else:  # process whole document
            pairs_list.append(pairs_)
            tokens_list.append(tokens_)

        # Always keeps the tags and spans at document level
        tags_list.append(tags_)
        spans_list.append(spans_)

        sub_corpus.addDocument(doc_id, doc)

    return pairs_list, tokens_list, tags_list, spans_list, sub_corpus, sent_ids, doc_bounds


def _dump_evaluation(golden_corpus, predicted_corpus, purpose_folder, result_file):
    # Compute accuracy, print out summary, serialize summary
    for eval_fashion in ["partial", "exact"]:
        perf = evaluation.evaluateAgreement(predicted_corpus, golden_corpus, eval_fashion)

        output_folder = os.path.join(ios.DATA_DIR, "results", purpose_folder)
        m_utils.safe_mkdir(output_folder)
        csv_out_file = os.path.join(output_folder, result_file + '_' + eval_fashion + ".csv")
        evaluation.writeCSV(perf, csv_out_file)
        pkl_out_file = os.path.join(output_folder, result_file + '_' + eval_fashion + ".pkl")
        with open(pkl_out_file, "wb") as f:
            pickle.dump(dict(perf), f)  # a defaultdict cannot be pickled, a dict can


def _evaluate(tokens, golden_tags, tagger, model, token_spans, C, doc_ids, result_file, train_or_test_folder, fold,
              purpose_folder, split_sents, doc_bounds, param_file):
    """
    Evaluate learnt models and print out results.

    :param tokens:
    :param golden_tags:
    :param tagger:
    :param model:
    :param token_spans:
    :param C:
    :param doc_ids: This is doc ID list. Don't replace by sentence ID list.
    :param result_file: (str) File to write evaluation measures.
    :param train_or_test_folder: (str) Folder to dump the predicted entities in CSV file format.
    :param fold:
    :param purpose_folder: For which purpose would data in this folder represent.
    :param split_sents:
    :param doc_bounds:
    :param sent_ids:
    :param param_file: Parameter file for the LSTM model.
    :return:
    """
    # Predict labels
    if tagger == crf:
        predicted_tags = tagger.tag(tokens, model, fold)
    elif tagger == lstm_neuroner:
        predicted_tags = tagger.tag(param_file, model, train_or_test_folder)
    elif tagger == ensembler:
        predicted_tags = model
    else:
        predicted_tags = tagger.tag(tokens if not param_file else param_file, model, fold)

    assert len(predicted_tags) == len(tokens)
    if not predicted_tags:
        print("Evaluation error: Tagger {0} produces no output".format(tagger.__name__))
        return

    # Concatenate tokens & predicted tags if the model was trained on sentence basis
    if split_sents:
        doc_tokens = list()
        doc_predicted_tags = list()
        for doc_idx in range(len(doc_bounds)):
            start = 0 if doc_idx == 0 else doc_bounds[doc_idx - 1]
            end = doc_bounds[doc_idx]
            # Concat sentence tokens
            tok_buffer = list()
            for sent_idx in range(start, end):
                tok_buffer.extend(tokens[sent_idx])
            doc_tokens.append(tok_buffer)
            # Concat sentence predicted tags
            tag_buffer = list()
            for sent_idx in range(start, end):
                tag_buffer.extend(predicted_tags[sent_idx])
            doc_predicted_tags.append(tag_buffer)
            # Check integrity
            assert len(tok_buffer) == len(tag_buffer)
            assert len(tok_buffer) == len(golden_tags[doc_idx])
        # Restore document level tokens & predicted tags
        tokens = doc_tokens
        predicted_tags = doc_predicted_tags

    # Check integrity
    assert len(tokens) == len(predicted_tags) == len(golden_tags) == len(token_spans)

    # Compute B/I/O accuracy
    print("Overall I/O/B accuracy = {0}".format(evaluation.evaluateBIOSequences(golden_tags, predicted_tags)))

    # Convert to entity objects
    golden_corpus = corpus.Corpus()
    predicted_corpus = corpus.Corpus()
    predicted_pairs = list()
    for doc_idx in range(len(predicted_tags)):
        sent_tags = predicted_tags[doc_idx]
        parsed_tags = [tags.parseTag(tag_str) for tag_str in sent_tags]
        spans = token_spans[doc_idx]
        coalesced_ents = tags.coalesceEntiyBIOTags(spans, parsed_tags)
        docID = doc_ids[doc_idx]
        golden_doc = C.getDocument(docID)
        golden_corpus.addDocument(docID, golden_doc)
        predicted_doc = corpus.DocumentFactory.createDocument(docID, golden_doc.text, coalesced_ents)
        predicted_corpus.addDocument(docID, predicted_doc)
        predicted_pairs.append(list(zip(tokens[doc_idx], predicted_tags[doc_idx])))

    # Compute accuracy, print out summary, serialize summary
    _dump_evaluation(golden_corpus, predicted_corpus, purpose_folder, result_file)

    # Dump prediction outcome to CSV format
    csv_format.csvSerializeCorpus(predicted_corpus,
                                  os.path.join(ios.DATA_DIR, csv_format.CSV_FOLDER, purpose_folder,
                                               "sent" if split_sents else "doc", str(fold), train_or_test_folder))

    # Dump prediction outcome to CoNLL format
    crf._exportAllFormat(fold, train_or_test_folder, predicted_pairs, doc_ids,
                         purpose_folder + '/' + ("sent" if split_sents else "doc"))


def testCRF(ENTITIES_TO_TAG, fold, split_sents, dataset_name=""):
    """
    Test the CRF classifier.

    :param tagger: Select which tagger engine to use.
    :param fold: Fold number of cross-validation.
    :param split_sents: Control whether to do sentence segmentation. False means to keep the whole document.
    :param dataset_name: (str) Name of the dataset. To be used to form deeper directory structure.
    :return:
    """
    # Config
    tokenizer = tokenizing.stanford_tokenizer
    # text_preprocessor = preprocessing.identifyOverRedaction
    text_preprocessor = None
    # tagger = crf  # ** Select which tagger engine to use **
    tagger_name = "crf"

    # Load a corpus
    # C, _ = _loadPostData(True, False)
    C = _loadGoldStandardData(True)

    # Split the corpus into training & testing data
    # only load existing data; load from the common cross-validation used in training
    train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, None, None, "")
    # trainval_doc_ids = train_doc_ids + val_doc_ids
    print("Fold {0}".format(fold))
    print("Num Train docs = {}, Num Test docs = {}".format(len(train_doc_ids), len(test_doc_ids)))

    # Prepare data
    # trainval_pairs, trainval_tokens, trainval_tags, trainval_spans, trainval_corpus, trainval_sent_ids, trainval_doc_bounds = \
    #     _prepare_data(C, trainval_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    train_pairs, train_tokens, train_tags, train_spans, train_corpus, train_sent_ids, train_doc_bounds = \
        _prepare_data(C, train_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    val_pairs, val_tokens, val_tags, val_spans, val_corpus, val_sent_ids, val_doc_bounds = \
        _prepare_data(C, val_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    test_pairs, test_tokens, test_tags, test_spans, test_corpus, test_sent_ids, test_doc_bounds = \
        _prepare_data(C, test_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)

    # Evaluate TRAINING accuracy
    print("Evaluate TRAINING accuracy:")
    start_time = timer()
    _evaluate(train_tokens, train_tags, crf, os.path.join(dataset_name, "sent" if split_sents else "doc"), train_spans,
              C, train_doc_ids,
              "eval_" + tagger_name + "_train_" + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().TRAIN_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name), split_sents,
              train_doc_bounds, None)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))

    # Evaluate VALIDATION accuracy
    print("Evaluate VALIDATION accuracy:")
    start_time = timer()
    _evaluate(val_tokens, val_tags, crf, os.path.join(dataset_name, "sent" if split_sents else "doc"), val_spans, C,
              val_doc_ids,
              "eval_" + tagger_name + "_val_" + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().VALIDATION_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name), split_sents,
              val_doc_bounds, None)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))

    # Evaluate TESTING accuracy
    print("Evaluate TESTING accuracy:")
    start_time = timer()
    _evaluate(test_tokens, test_tags, crf, os.path.join(dataset_name, "sent" if split_sents else "doc"), test_spans, C,
              test_doc_ids,
              "eval_" + tagger_name + "_test_" + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().TEST_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name), split_sents,
              test_doc_bounds, None)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))


def testLSTM_NeuroNER(ENTITIES_TO_TAG, fold, split_sents, embedding, dataset_name):
    """
    Testing a NeuroNER pre-trained model.

    :return:
    """
    # Config
    # tokenizer = TreebankWordTokenizer2()
    tokenizer = tokenizing.stanford_tokenizer
    # text_preprocessor = preprocessing.identifyOverRedaction
    text_preprocessor = None
    # tagger = crf  # ** Select which tagger engine to use **
    tagger_name = "neuroner"

    # Load a corpus
    C = _loadGoldStandardData(True)

    # Split the corpus into training & testing data
    train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, False, None, "")
    print("Fold {0}".format(fold))
    print("Num Train docs = {0}, Num Val docs = {1}, Num Test docs = {2}".format(
        len(train_doc_ids), len(val_doc_ids), len(test_doc_ids)))

    ## Prepare data
    train_pairs, train_tokens, train_tags, train_spans, train_corpus, train_sent_ids, train_doc_bounds = \
        _prepare_data(C, train_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    val_pairs, val_tokens, val_tags, val_spans, val_corpus, val_sent_ids, val_doc_bounds = \
        _prepare_data(C, val_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    test_pairs, test_tokens, test_tags, test_spans, test_corpus, test_sent_ids, test_doc_bounds = \
        _prepare_data(C, test_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)

    # Get parameter file
    param_file = lstm_neuroner.parameter_filepath(embedding, split_sents, fold)

    # Evaluate TRAINING accuracy
    print("Evaluate TRAINING accuracy:")
    start_time = timer()
    # Note: pass dataset name in place of model, so that NeuroNER replaces the 'rundata' pseudo name by the correct dataset name
    _evaluate(train_tokens, train_tags, lstm_neuroner, dataset_name, train_spans, C, train_doc_ids,
              "eval_" + tagger_name + "_train_" + embedding + '_' + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().TRAIN_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name, embedding),
              split_sents, train_doc_bounds, param_file)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))

    # Evaluate VALIDATION accuracy
    print("Evaluate VALIDATION accuracy:")
    start_time = timer()
    # Note: pass dataset name in place of model, so that NeuroNER replaces the 'rundata' pseudo name by the correct dataset name
    _evaluate(val_tokens, val_tags, lstm_neuroner, dataset_name, val_spans, C, val_doc_ids,
              "eval_" + tagger_name + "_val_" + embedding + '_' + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().VALIDATION_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name, embedding),
              split_sents, val_doc_bounds, param_file)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))

    # Evaluate TESTING accuracy
    print("Evaluate TESTING accuracy:")
    start_time = timer()
    # Note: pass dataset name in place of model, so that NeuroNER replaces the 'rundata' pseudo name by the correct dataset name
    _evaluate(test_tokens, test_tags, lstm_neuroner, dataset_name, test_spans, C, test_doc_ids,
              "eval_" + tagger_name + "_test_" + embedding + '_' + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().TEST_SUBFOLDER, fold, os.path.join(tagger_name, dataset_name, embedding),
              split_sents, test_doc_bounds, param_file)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))


def testEnsembler(dataset_name, ENTITIES_TO_TAG, fold, classifier, feat_window_size):
    """
    Train and test performance of the ensemble tagger.

    :param classifier: (str) Softmax / ECOC
    :param feat_window_size: (int) Width of the feature window
    :return:
    """
    # Fixed parameters
    split_sents = False  # ensemble tagging works at document level
    feat_encoding_style = ensembler.EncodingStyle.OneHot  # lets reason if Number encoding style is good for feature?

    print(
        "\nStacking ensemble learning: \n - Dataset = {0} \n - Entities =  {1} \n - Classifier = {2} \n - Feature window = {3} \n - Fold = {4}".format(
            dataset_name, ENTITIES_TO_TAG, classifier, feat_window_size, fold))

    # Load tags
    val_tags_crf_sent = ensembler.loadCRFtags(dataset_name, True, fold, "val")
    val_tags_crf_doc = ensembler.loadCRFtags(dataset_name, False, fold, "val")
    val_tags_neuroner_pubmed_doc = ensembler.loadNeuroNERtags(dataset_name, "pubmed", False, fold, "val")
    val_tags_neuroner_pubmed_sent = ensembler.loadNeuroNERtags(dataset_name, "pubmed", True, fold, "val")
    val_tags_neuroner_wiki_doc = ensembler.loadNeuroNERtags(dataset_name, "wiki", False, fold, "val")
    val_tags_neuroner_wiki_sent = ensembler.loadNeuroNERtags(dataset_name, "wiki", True, fold, "val")
    val_tags_gold = ensembler.loadGoldStandardTags(dataset_name, fold, "val")

    test_tags_crf_sent = ensembler.loadCRFtags(dataset_name, True, fold, "test")
    test_tags_crf_doc = ensembler.loadCRFtags(dataset_name, False, fold, "test")
    test_tags_neuroner_pubmed_doc = ensembler.loadNeuroNERtags(dataset_name, "pubmed", False, fold, "test")
    test_tags_neuroner_pubmed_sent = ensembler.loadNeuroNERtags(dataset_name, "pubmed", True, fold, "test")
    test_tags_neuroner_wiki_doc = ensembler.loadNeuroNERtags(dataset_name, "wiki", False, fold, "test")
    test_tags_neuroner_wiki_sent = ensembler.loadNeuroNERtags(dataset_name, "wiki", True, fold, "test")
    test_tags_gold = ensembler.loadGoldStandardTags(dataset_name, fold, "test")

    # Note: ensembler 2 = crf_sent + neuroner_pubmed_doc
    #       ensembler 6 = 2 crf + 4 neuroner

    # Train and test a stacking ensemble classifier
    individual_entity_predictions = list()
    for ent_type in ENTITIES_TO_TAG:
        X_train, y_train = ensembler.collectDataset([val_tags_crf_sent, val_tags_neuroner_pubmed_doc,
                                                     val_tags_crf_doc, val_tags_neuroner_pubmed_sent,
                                                     val_tags_neuroner_wiki_doc, val_tags_neuroner_wiki_sent],
                                                    val_tags_gold, ent_type, feat_window_size, feat_encoding_style)
        clf, train_acc = ensembler.trainStackingEnsembler([X_train, y_train], classifier)

        X_test, y_test = ensembler.collectDataset([test_tags_crf_sent, test_tags_neuroner_pubmed_doc,
                                                   test_tags_crf_doc, test_tags_neuroner_pubmed_sent,
                                                   test_tags_neuroner_wiki_doc, test_tags_neuroner_wiki_sent],
                                                  test_tags_gold, ent_type, feat_window_size, feat_encoding_style)
        test_acc = clf.score(X_test, y_test)
        prediction = ensembler.predict(clf, X_test, ent_type)
        individual_entity_predictions.append(prediction)

    # Convert the output back to same format with the evaluator
    joined_predictions = ensembler.joinPrediction(individual_entity_predictions)
    joined_predictions = ensembler.segmentBackIntoDocuments(joined_predictions, test_tags_gold)

    # Init a regular evaluation process
    tokenizer = tokenizing.stanford_tokenizer
    text_preprocessor = None
    tagger_name = "ensembler"

    C = _loadGoldStandardData(True)
    train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, None, None, "")
    print("Fold {0}".format(fold))
    print("Num Train docs = {}, Num Test docs = {}".format(len(train_doc_ids), len(test_doc_ids)))

    # Make sure the document IDs are the same
    assert set(test_doc_ids) == set(test_tags_gold.keys())  # doc IDs might have different ordering in the 2 collections
    test_doc_ids = list(test_tags_gold.keys())

    # Prepare data
    # trainval_pairs, trainval_tokens, trainval_tags, trainval_spans, trainval_corpus, trainval_sent_ids, trainval_doc_bounds = \
    #     _prepare_data(C, trainval_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    train_pairs, train_tokens, train_tags, train_spans, train_corpus, train_sent_ids, train_doc_bounds = \
        _prepare_data(C, train_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    # val_pairs, val_tokens, val_tags, val_spans, val_corpus, val_sent_ids, val_doc_bounds = \
    #     _prepare_data(C, val_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    test_pairs, test_tokens, test_tags, test_spans, test_corpus, test_sent_ids, test_doc_bounds = \
        _prepare_data(C, test_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)

    # Evaluate testing accuracy
    print("Evaluate testing accuracy:")
    start_time = timer()
    _evaluate(test_tokens, test_tags, ensembler, joined_predictions,
              test_spans, C, test_doc_ids,
              "eval_" + tagger_name + "_test_" + ("sent" if split_sents else "doc") + '_' + str(fold),
              config.getConfig().TEST_SUBFOLDER, fold,
              os.path.join(tagger_name, dataset_name, classifier, "window_" + str(feat_window_size)),
              split_sents, test_doc_bounds, None)
    end_time = timer()
    print("Evaluation time = {0} seconds".format(end_time - start_time))


def exportCoNLL(ENTITIES_TO_TAG, fold, split_sents, dataset_name=""):
    """
    Export all train/test data in CoNLL format for benchmarking.

    :param fold: (int) Fold number of cross-validation.
    :param split_sents: (bool) Whether or not to perform sentence segmentation.
    :param dataset_name: (str) Name of the dataset. To be used to form deeper directory structure.
        If "jona", then load the first corpus (Jona)
        If "peishu", then load the second corpus (Pei-Shu)
        If "parallel", then load both into a parallel corpus
    :return:
    """
    # Config
    tokenizer = tokenizing.stanford_tokenizer
    text_preprocessor = None

    # Load a corpus
    if dataset_name.startswith("gsc"):  # gold standard corpus
        C = _loadGoldStandardData(True)
        print("Loaded a Gold Standard Corpu of {} documents".format(len(C.m_docs)))
        # Split the corpus into training & testing data
        train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, True, None)
        print("Fold {0}".format(fold))
        print("Num Train docs = {0}, Num Val docs = {1}, Num Test docs = {2}".format(
            len(train_doc_ids), len(val_doc_ids), len(test_doc_ids)))
    elif dataset_name.startswith("jona") or dataset_name.startswith("peishu"):  # single corpus
        C1, C2 = _loadPostData(True, False)
        if dataset_name.startswith("jona"):
            C = C1
        elif dataset_name.startswith("peishu"):
            C = C2
        else:
            print("Error: Unrecognized corpus ID.")
            return

        print("Loaded a individual corpus of {} documents".format(len(C.m_docs)))
        # Split the corpus into training & testing data
        train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, True, None)
        print("Fold {0}".format(fold))
        print("Num Train docs = {0}, Num Val docs = {1}, Num Test docs = {2}".format(
            len(train_doc_ids), len(val_doc_ids), len(test_doc_ids)))
    elif dataset_name.startswith("parallel"):  # parallel corpus
        C, num_parallel_corpuses = _loadPostData(True, True)
        print("Loaded {} parallel corpuses of {} documents".format(num_parallel_corpuses, len(C.m_docs)))
        # Split the corpus into training & testing data
        train_doc_ids, val_doc_ids, test_doc_ids = _split_traintest(C, fold, True, num_parallel_corpuses)
        print("Fold {0}".format(fold))
        print("Num Train docs = {0}, Num Val docs = {1}, Num Test docs = {2}".format(
            len(train_doc_ids), len(val_doc_ids), len(test_doc_ids)))
    else:
        print("Error: Unrecognized dataset name.")
        return

    # Prepare training data
    train_pairs, train_tokens, train_tags, train_spans, train_corpus, train_sent_ids, train_doc_bounds = \
        _prepare_data(C, train_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    # Export training data
    print("Export training data to CoNLL format")
    crf._exportAllFormat(fold, config.getConfig().TRAIN_SUBFOLDER, train_pairs,
                         train_sent_ids if split_sents else train_doc_ids,
                         os.path.join('conll', dataset_name, "sent" if split_sents else "doc"))
    print("Export training data to CSV format")
    csv_format.csvSerializeCorpus(train_corpus,
                                  os.path.join(ios.DATA_DIR, "annotations_csv", 'conll', dataset_name,
                                               "sent" if split_sents else "doc",
                                               str(fold), config.getConfig().TRAIN_SUBFOLDER))

    # Prepare validation data
    val_pairs, val_tokens, val_tags, val_spans, val_corpus, val_sent_ids, val_doc_bounds = \
        _prepare_data(C, val_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    # Export validation data
    print("Export validation data to CoNLL format")
    crf._exportAllFormat(fold, config.getConfig().VALIDATION_SUBFOLDER, val_pairs,
                         val_sent_ids if split_sents else val_doc_ids,
                         os.path.join('conll', dataset_name, "sent" if split_sents else "doc"))
    print("Export validation data to CSV format")
    csv_format.csvSerializeCorpus(val_corpus,
                                  os.path.join(ios.DATA_DIR, "annotations_csv", 'conll', dataset_name,
                                               "sent" if split_sents else "doc",
                                               str(fold), config.getConfig().VALIDATION_SUBFOLDER))

    # Prepare test data
    test_pairs, test_tokens, test_tags, test_spans, test_corpus, test_sent_ids, test_doc_bounds = \
        _prepare_data(C, test_doc_ids, ENTITIES_TO_TAG, tokenizer, text_preprocessor, split_sents)
    # Export test data
    print("Export test data to CoNLL format")
    crf._exportAllFormat(fold, config.getConfig().TEST_SUBFOLDER, test_pairs,
                         test_sent_ids if split_sents else test_doc_ids,
                         os.path.join('conll', dataset_name, "sent" if split_sents else "doc"))
    print("Export test data to CSV format")
    csv_format.csvSerializeCorpus(test_corpus,
                                  os.path.join(ios.DATA_DIR, "annotations_csv", 'conll', dataset_name,
                                               "sent" if split_sents else "doc",
                                               str(fold), config.getConfig().TEST_SUBFOLDER))


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--test-neuroner", action="store_true", default=False)
    parser.add_argument("--test-crf", action="store_true", default=False)
    parser.add_argument("--export-conll", action="store_true", default=False)
    parser.add_argument("--test-ensembler", action="store_true", default=False)
    args = parser.parse_args(argv)

    if args.test_neuroner:
        for ent_types in ENTITIES_TO_TAG:
            for annotator in ['gsc']:
                dataset_name = annotator + '_' + ent_types
                for embedding in ["wiki", "pubmed"]:
                    for split_sent in [False, True]:
                        for fold in [1, 2, 3, 4, 5]:
                            testLSTM_NeuroNER(ENTITIES_TO_TAG[ent_types], fold, split_sent, embedding, dataset_name)

    if args.test_crf:
        for ent_types in ENTITIES_TO_TAG:
            for annotator in ['gsc']:
                dataset_name = annotator + '_' + ent_types
                for split_sent in [False, True]:
                    for fold in [1, 2, 3, 4, 5]:
                        testCRF(ENTITIES_TO_TAG[ent_types], fold, split_sent, dataset_name)

    if args.test_ensembler:
        for ent_types in ["all_ents"]:
            for annotator in ['gsc']:
                dataset_name = annotator + '_' + ent_types
                for classifier in ["ECOC", "Softmax"]:
                    for feat_window_size in range(1, 40, 2):
                        for fold in [1, 2, 3, 4, 5]:
                            testEnsembler(dataset_name, ENTITIES_TO_TAG[ent_types], fold, classifier, feat_window_size)

    if args.export_conll:
        for ent_types in ENTITIES_TO_TAG:
            for annotator in ['gsc']:
                dataset_name = annotator + '_' + ent_types
                for fold in [1, 2, 3, 4, 5]:
                    for split_sentences in [True, False]:
                        exportCoNLL(ENTITIES_TO_TAG[ent_types], fold, split_sentences, dataset_name)


if __name__ == "__main__":
    main(sys.argv[1:])
