"""
Author: Thanh Thieu

Facilities to corss-validate the train/test dataset.
A cross-validation setup is divided into 2 folders: 1 for training, and 1 for testing.
Inside each folder are files containing document IDs for each fold.
Fold numbering starts from 1.
"""

import os
from enum import Enum
import csv
from sklearn.model_selection import KFold
import shutil
import numpy as np

import ios
import m_utils

ROOT_FOLDER = "cross_validation"
DATASET_NAME = ""  # Set this variable for deeper folder structure
TRAIN_FOLDER = "train"
VALIDATION_FOLDER = "val"
TEST_FOLDER = "test"
FOLD_BASE_NAME = "fold"
FILE_EXTENSION = "txt"
FOLD_NUMBER_START = 1
MAX_NUM_FOLDS = 5
CSV_DELIMITER = ','


def setDatasetName(name):
    """
    Giving name of a dataset will create a deeper folder structure.

    :param name: (str)
    :return:
    """
    global DATASET_NAME
    DATASET_NAME = name


def _trainFolderPath():
    """
    Compose and return the path to the train folder.
    Also check for path existence and create one if not.
    :return:
    """
    path = os.path.join(ios.DATA_DIR, ROOT_FOLDER, DATASET_NAME, TRAIN_FOLDER)
    if not os.path.exists(path):
        os.makedirs(path)

    return path


def _validationFolderPath():
    """
    Compose and return the path to the validation folder.
    Also check for path existence and create one if not.
    :return:
    """
    path = os.path.join(ios.DATA_DIR, ROOT_FOLDER, DATASET_NAME, VALIDATION_FOLDER)
    if not os.path.exists(path):
        os.makedirs(path)

    return path


def _testFolderPath():
    """
    Compose and return the path to the test folder.
    :return:
    """
    path = os.path.join(ios.DATA_DIR, ROOT_FOLDER, DATASET_NAME, TEST_FOLDER)
    if not os.path.exists(path):
        os.makedirs(path)

    return path


def _fileName(fold_number):
    """
    Compose and return the file name for a specific fold.
    :param fold_number: (int)
        Fold number.
    :return:
    """
    return FOLD_BASE_NAME + str(fold_number) + '.' + FILE_EXTENSION


class Group(Enum):
    """
    Identity of the train/test group of example.
    """
    Train = 0
    Val = 1
    Test = 2


def _filePath(group, fold):
    """
    Compose and return a complete path to a file of a specified train/test group and a specified fold.
    :param group: (Group)
    :param fold: (int)
    :return:
    """
    if group == Group.Train:
        return os.path.join(_trainFolderPath(), _fileName(fold))
    elif group == Group.Val:
        return os.path.join(_validationFolderPath(), _fileName(fold))
    elif group == Group.Test:
        return os.path.join(_testFolderPath(), _fileName(fold))
    else:
        raise ValueError("Un-recognizable train/test group.")


def _serialize(IDs, file_path):
    """
    Write a list of document IDs to a file.
    :param IDs:
    :param file_path:
    :return:
    """
    with open(file_path, 'w', newline='') as f:
        csvwriter = csv.writer(f, delimiter=CSV_DELIMITER)
        csvwriter.writerow(IDs)


def _deserialize(file_path):
    """
    Read a list of document IDs from a file.
    :param file_path:
    :return:
        A list/array of document IDs.
    """
    with open(file_path, newline='') as f:
        csvreader = csv.reader(f, delimiter=CSV_DELIMITER)
        count = 0
        for row in csvreader:
            count += 1
        if count != 1:
            raise IOError("Unexpected serialization format.")

        return row


def countFolds():
    """
    Count the number of folds already exists on the hard drive.
    :return: (int)
        Number of folds, or 0 to indicate there is either no data or inconsistent data.
    """
    train_folder = _trainFolderPath()
    train_files = [file_name for file_name in os.listdir(train_folder)
                   if os.path.isfile(os.path.join(train_folder, file_name))
                   and file_name.startswith(FOLD_BASE_NAME)]
    test_folder = _testFolderPath()
    test_files = [file_name for file_name in os.listdir(test_folder)
                  if os.path.isfile(os.path.join(test_folder, file_name))
                  and file_name.startswith(FOLD_BASE_NAME)]
    sym_diff = set(train_files) ^ set(test_files)

    if len(sym_diff) > 0:  # Inconsistent data
        return 0
    else:
        return len(train_files)


def splitData(IDs, num_folds, num_parallel_corpuses):
    """
    Write new cross-validate files to hard drive. Also wipe out all existing files.
    For example, num_folds = 5 means 20% is test, 20% is val, and 60% is train.

    :param IDs: (iterable) A list/array of document IDs of the entire dataset.
    :param num_folds: (int) Number of fold to split train/val/test sets.
    :param num_parallel_corpuses: (int) Number of parallel corpuses presented in the document IDs.
    :return:
    """
    shutil.rmtree(_trainFolderPath())
    shutil.rmtree(_testFolderPath())

    if num_parallel_corpuses and num_parallel_corpuses > 1:
        unique_IDs = {docID for (docID, corpusID) in [m_utils.parseParallelDocumentID(parallelID) for parallelID in IDs]}
        IDs = list(unique_IDs)

    IDs = np.array(IDs)
    # for _ in range(2):  # pre-shuffle several times to minimize pseudo-random effect
    #     np.random.shuffle(IDs)
    kf = KFold(n_splits=num_folds, shuffle=True, random_state=100) # same seed of the random sequence all the times
    fold = FOLD_NUMBER_START
    for val_train_indices, test_indices in kf.split(IDs):
        test_size = len(test_indices)
        val_indices = val_train_indices[:test_size]
        train_indices = val_train_indices[test_size:]

        trainIDs = IDs[train_indices]
        valIDs = IDs[val_indices]
        testIDs = IDs[test_indices]

        if num_parallel_corpuses and num_parallel_corpuses > 1:
            trainIDs = m_utils.generateParallelDocumentIDs(trainIDs, num_parallel_corpuses)
            valIDs = m_utils.generateParallelDocumentIDs(valIDs, num_parallel_corpuses)
            testIDs = m_utils.generateParallelDocumentIDs(testIDs, num_parallel_corpuses)

        _serialize(trainIDs, _filePath(Group.Train, fold))
        _serialize(valIDs, _filePath(Group.Val, fold))
        _serialize(testIDs, _filePath(Group.Test, fold))
        fold += 1


def getFoldData(fold):
    """
    Get train and test document IDs for a specified fold.
    :param fold:
    :return:
        A tuple of lists of the form ([train IDs], [val IDs], [test IDs]).
    """
    train_ids = _deserialize(_filePath(Group.Train, fold))
    val_ids = _deserialize(_filePath(Group.Val, fold))
    test_ids = _deserialize(_filePath(Group.Test, fold))
    return train_ids, val_ids, test_ids
