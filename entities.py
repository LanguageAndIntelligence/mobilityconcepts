"""
@Author:
    Thanh Thieu

Define all entity types of the framework.
"""

from attributes import *


class Entity:
    """
    A base class for any type of Entities.
    """

    def __init__(self, textspan):
        """
        Create a base entity mention.
        :param textspan: TextSpan
            Keep indices of first and last characters of the textual span of this entity
        """
        self.m_textspan = textspan

    @property
    def first_char_index(self):
        return self.m_textspan.m_firstchar_index

    @property
    def last_char_index(self):
        return self.m_textspan.m_lastchar_index

    @property
    def text(self):
        return self.m_textspan.m_text

    @property
    def tokens(self):
        return self.m_textspan.m_tokens

    @property
    def textspan(self):
        return self.m_textspan

    @textspan.setter
    def textspan(self, new_textspan):
        self.m_textspan = new_textspan


class ScoreDefinition(Entity):
    """
    Score Definition entity.
    """

    def __init__(self, textspan, att_mapping=None):
        """
        Create a score definition entity mention.
        :param textspan: TextSpan
            Keep indices of first and last characters of the textual span of this entity
        :param att_mapping:
            No use. Just to be conforming with other entities' constructors.
        """
        super().__init__(textspan)

    @staticmethod
    def attributeTypes():
        """
        Get types of attributes belonging to this entity type.
        :return: 
        """
        return tuple()

    def equals(self, other, compare_attributes=True):
        """
        Check if two Score Definition entities have the same contents.

        :param other:
        :param compare_attributes: If True then also compare attribute values.
        :return: True/False
        """
        if self.m_textspan != other.m_textspan:
            return False

        # No attribute to compare

        return True

    def __eq__(self, other):
        """
        Check if two Score Definition entities have the same contents.

        :param other:
        :return: True/False
        """
        return self.equals(other, compare_attributes=True)


class Action(Entity):
    """
    Action entity.
    """

    def __init__(self, textspan, code=None, polarity=None, att_mapping=None):
        """
        Create an Action entity mention.
        :param textspan: TextSpan
        :param code: ICFCode
        :param polarity: ActionPolarity
        :param att_mapping: dict
            A dictionary mapping from attribute types to attribute values.
            An alternative to listing individual attributes.
        """
        super().__init__(textspan)
        self.m_code = code
        self.m_polarity = polarity
        if att_mapping:
            if ICFCode in att_mapping:
                self.m_code = att_mapping[ICFCode]
            if ActionPolarity in att_mapping:
                self.m_polarity = att_mapping[ActionPolarity]

    def getAttribute(self, attribute_type):
        """
        Get value of an attribute given its type.
        :param attribute_type: 
        :return: 
        """
        if attribute_type == ICFCode:
            return self.m_code
        elif attribute_type == ActionPolarity:
            return self.m_polarity
        else:
            raise TypeError("Cannot recognize attribute type")

    @staticmethod
    def attributeTypes():
        """
        Get types of attributes belonging to this entity type.
        :return: 
        """
        return ICFCode, ActionPolarity

    def equals(self, other, compare_attributes=True):
        """
        Check if two Action entities have the same contents.

        :param other:
        :param compare_attributes: If True then also compare attribute values.
        :return: True/False
        """
        if self.m_textspan != other.m_textspan:
            return False

        if compare_attributes:
            for attribute_type in Action.attributeTypes():
                if self.getAttribute(attribute_type) != other.getAttribute(attribute_type):
                    return False

        return True

    def __eq__(self, other):
        """
        Check if two Action entities have the same contents.
        :param other:
        :return: True/False
        """
        self.equals(other, compare_attributes=True)


class Assistance(Entity):
    """
    Assistance entity.
    """

    def __init__(self, textspan, polarity=None, source=None, att_mapping=None):
        """
        Create an Assistance entity mention.
        :param textspan: TextSpan
        :param polarity: AssistancePolarity
        :param source: Source
        :param att_mapping: dict
            A dictionary mapping from attribute types to attribute values.
            An alternative to listing individual attributes.
        """
        super().__init__(textspan)
        self.m_polarity = polarity
        self.m_source = source
        if att_mapping:
            if AssistancePolarity in att_mapping:
                self.m_polarity = att_mapping[AssistancePolarity]
            if Source in att_mapping:
                self.m_source = att_mapping[Source]

    def getAttribute(self, attribute_type):
        """
        Get value of an attribute given its type.
        :param attribute_type: 
        :return: 
        """
        if attribute_type == AssistancePolarity:
            return self.m_polarity
        elif attribute_type == Source:
            return self.m_source
        else:
            raise TypeError("Cannot recognize attribute type")

    @staticmethod
    def attributeTypes():
        """
        Get types of attributes belonging to this entity type.
        :return: 
        """
        return AssistancePolarity, Source

    def equals(self, other, compare_attributes=True):
        """
        Check if two Assistance entities have the same contents.

        :param other:
        :param compare_attributes: If True then also compare attribute values.
        :return: True/False
        """
        if self.m_textspan != other.m_textspan:
            return False

        if compare_attributes:
            for attribute_type in Assistance.attributeTypes():
                if self.getAttribute(attribute_type) != other.getAttribute(attribute_type):
                    return False

        return True

    def __eq__(self, other):
        """
        Check if two Assistance entities have the same contents.
        :param other:
        :return: True/False
        """
        self.equals(other, compare_attributes=True)


class Quantification(Entity):
    """
    Quantification entity.
    """

    def __init__(self, textspan, type=None, att_mapping=None):
        """
        Create a Quantification entity mention.
        :param textspan: TextSpan
        :param type: QuantificationType
        :param att_mapping: dict
            A dictionary mapping from attribute types to attribute values.
            An alternative to listing individual attributes.
        """
        super().__init__(textspan)
        self.m_type = type
        if att_mapping:
            if QuantificationType in att_mapping:
                self.m_type = att_mapping[QuantificationType]

    def getAttribute(self, attribute_type):
        """
        Get value of an attribute given its type.
        :param attribute_type: 
        :return: 
        """
        if attribute_type == QuantificationType:
            return self.m_type
        else:
            raise TypeError("Cannot recognize attribute type")

    @staticmethod
    def attributeTypes():
        """
        Get types of attributes belonging to this entity type.
        :return: 
        """
        return QuantificationType,

    def equals(self, other, compare_attributes=True):
        """
        Check if two Quantification entities have the same contents.

        :param other:
        :param compare_attributes: If True then also compare attribute values.
        :return: True/False
        """
        if self.m_textspan != other.m_textspan:
            return False

        if compare_attributes:
            for attribute_type in Quantification.attributeTypes():
                if self.getAttribute(attribute_type) != other.getAttribute(attribute_type):
                    return False

        return True

    def __eq__(self, other):
        """
        Check if two Quantification entities have the same contents.
        :param other:
        :return: True/False
        """
        self.equals(other, compare_attributes=True)


class Mobility(Entity):
    """
    Mobility entity.
    """

    def __init__(self, textspan, type=None, subject=None, history=None, att_mapping=None,
                 actions=None, assistances=None, quantifications=None):
        """
        Create a Mobility entity mention.
        :param textspan: TextSpan
        :param type: MobilityType
        :param subject: Subject
        :param history: History
        :param actions: A list of Action entities
        :param assistances: A list of Assistance entities
        :param quantifications: A list of Quantification entities
        :param att_mapping: dict
            A dictionary mapping from attribute types to attribute values.
            An alternative to listing individual attributes.
        """
        super().__init__(textspan)
        # Attributes
        self.m_type = type
        self.m_subject = subject
        self.m_history = history
        if att_mapping:
            if MobilityType in att_mapping:
                self.m_type = att_mapping[MobilityType]
            if Subject in att_mapping:
                self.m_subject = att_mapping[Subject]
            if History in att_mapping:
                self.m_history = att_mapping[History]
        # Sub entity lists
        self.m_actions = list() if actions == None else actions
        self.m_assistances = list() if assistances == None else assistances
        self.m_quantifications = list() if quantifications == None else quantifications

    def addSubEntity(self, entity_type, ent):
        """
        Add a sub-entity into this Mobility.

        :param entity_type: Type of sub-entity.
        :param ent: The sub-entity itself.
        :return: None
        """
        if entity_type == Action:
            self.m_actions.append(ent)
        elif entity_type == Assistance:
            self.m_assistances.append(ent)
        elif entity_type == Quantification:
            self.m_quantifications.append(ent)
        else:
            raise TypeError("Cannot recognize entity type: " + entity_type.__name__)

    def getSubEntities(self, entity_type):
        """
        Get sub-entities of a specified type.

        :param entity_type: Type of sub-entity.
        :return: (list) A list of sub-entities.
        """
        if entity_type == Action:
            return self.m_actions
        elif entity_type == Assistance:
            return self.m_assistances
        elif entity_type == Quantification:
            return self.m_quantifications
        else:
            raise TypeError("Cannot recognize entity type: " + entity_type.__name__)

    def getAttribute(self, attribute_type):
        """
        Get value of an attribute given its type.
        :param attribute_type: 
        :return: 
        """
        if attribute_type == MobilityType:
            return self.m_type
        elif attribute_type == Subject:
            return self.m_subject
        elif attribute_type == History:
            return self.m_history
        else:
            raise TypeError("Cannot recognize attribute type")

    @staticmethod
    def attributeTypes():
        """
        Get types of attributes belonging to this entity type.
        :return: 
        """
        return MobilityType, Subject, History

    def equals(self, other, compare_attributes=True):
        """
        Check if two Mobility entities have the same contents.

        :param other:
        :param compare_attributes: If True then also compare attribute values.
        :return: True/False
        """
        if self.m_textspan != other.m_textspan:
            return False

        if compare_attributes:
            for attribute_type in Mobility.attributeTypes():
                if self.getAttribute(attribute_type) != other.getAttribute(attribute_type):
                    return False

        # Assume the lists of sub-entities have been already sorted
        for sub_entity_type in subEntityTypesOfMobility():
            subents1 = self.getSubEntities(sub_entity_type)
            subents2 = other.getSubEntities(sub_entity_type)
            if len(subents1) != len(subents2):
                return False
            for i in range(len(subents1)):
                if not subents1[i].equals(subents2[i], compare_attributes):  # inherit compare_attribute from Mobility
                    return False

        return True

    def __eq__(self, other):
        """
        Check if two Mobility entities have the same contents.
        :param other:
        :return: True/False
        """
        self.equals(other, compare_attributes=True)


# -----------------
# Helper functions
# -----------------


def allEntityTypes():
    """
    Return the list of entity types of interest in this module.
    :return: 
    """
    return Mobility, Action, Assistance, Quantification, ScoreDefinition


def subEntityTypesOfMobility():
    """
    Return types of entities that can be sub-entities of a Mobility entity.
    :return:
    """
    return Action, Assistance, Quantification
