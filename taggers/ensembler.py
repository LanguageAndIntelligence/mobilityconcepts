"""
Author: Thanh Thieu

This module performs ensemble learning.
"""

import os
import pandas as pd
import numpy as np
import sys
from enum import Enum
from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OutputCodeClassifier
from sklearn.svm import LinearSVC

import config
import m_utils
import tags

EXCLUDE_FILES = {"data.col"}

KEY_TOKENS = "tokens"
KEY_TAGS = "tags"


def _loadCoNLLtags(base_dir):
    """
    Load the entire folder of column files (".col") as a dataset.

    :param base_dir: The folder that contain column files.
    :return:
    """
    dataset = dict()
    for file_name in m_utils.get_files(base_dir):
        if file_name.endswith("col") and (not file_name in EXCLUDE_FILES):
            doc_id = os.path.splitext(file_name)[0]
            data = pd.read_csv(os.path.join(base_dir, file_name), sep=r"\s+", header=None)
            tokens = list(data[0])
            tags = list(data[1])
            dataset[doc_id] = {KEY_TOKENS: tokens, KEY_TAGS: tags}

    return dataset


def _parseTags(dataset):
    """
    Parse string representation of tags into (tag_type, entity_type) pairs.

    :param dataset:
    :return: Modify the the input dataset argument to contains parsed tags.
    """
    for doc_id in dataset:
        parsed_tags = [tags.parseTag(tag_str) for tag_str in dataset[doc_id][KEY_TAGS]]
        dataset[doc_id][KEY_TAGS] = parsed_tags

    return dataset


def loadCRFtags(dataset_name, split_sent, fold, crossval):
    """
    Load tagging results of the CRF model.

    :param dataset_name: (str)
    :param split_sent: True/False
    :param fold: (int)
    :param crossval: (str) train / val / test
    :return:
    """
    base_dir = os.path.join(config.getConfig().TAG_DIR_CRF, dataset_name, "sent" if split_sent else "doc",
                            str(fold), crossval)
    return _parseTags(_loadCoNLLtags(base_dir))


def loadNeuroNERtags(dataset_name, embedding, split_sent, fold, crossval):
    """
    Load tagging results of the NeuroNER model.

    :param dataset_name: (str)
    :param embedding: (str) pubmed / wiki
    :param split_sent: True/False
    :param fold: (int)
    :param crossval: (str) train / val / test
    :return:
    """
    base_dir = os.path.join(config.getConfig().TAG_DIR_NeuroNER, dataset_name, embedding, "sent" if split_sent else "doc",
                            str(fold), crossval)
    return _parseTags(_loadCoNLLtags(base_dir))


def loadGoldStandardTags(dataset_name, fold, crossval):
    """
    Load tag sequence of the gold standard data. Always load from whole documents.

    :param dataset_name: (str)
    :param fold: (int)
    :param crossval: (str) train / val / test
    :return:
    """
    base_dir = os.path.join(config.getConfig().TAG_DIR_GOLD, dataset_name, "doc", str(fold), crossval)
    return _parseTags(_loadCoNLLtags(base_dir))


class EncodingStyle(Enum):
    Number = 0
    OneHot = 1


def _encodeBIOFeature(bio_tag, encoding_style):
    """
    Return numerical encoding of a B/I/O tag.
    :param bio_tag: (tags.NETags)
    :param encoding_style: (EncodingStyle enum)
    :return:
    """
    if bio_tag is None:  # paddings
        if encoding_style == EncodingStyle.Number:
            return [-1]
        elif encoding_style == EncodingStyle.OneHot:
            return [0] * 3
        else:
            print("Unrecognizable encoding style! Halting.")
            sys.exit(1)
    else:  # bio_tag not None
        if encoding_style == EncodingStyle.Number:
            return [bio_tag.value]
        elif encoding_style == EncodingStyle.OneHot:
            feat = [0] * 3
            feat[bio_tag.value] = 1
            return feat
        else:
            print("Unrecognizable encoding style! Halting.")
            sys.exit(1)


def _extractFeatureWindow(tag_seq, feat_pos, window_size, entity_type, encoding_style):
    """
    Extract a window of features from a tag sequence.
    :param tag_seq: A sequence of tags.
    :param feat_pos: Position in the tag sequence to extract features.
    :param window_size: Size of the feature window; should be an odd number.
    :param entity_type: The type of entity of interest.
    :param encoding_style: (EncodingStyle enum)
    :return:
    """
    if window_size % 2 == 0:
        print("Feature window size should be an odd number! Halting.")
        sys.exit(1)

    features = list()
    for abs_pos in range(feat_pos - window_size // 2, feat_pos + window_size // 2 + 1):  # absolute position
        feat = None
        if abs_pos < 0 or abs_pos >= len(tag_seq):  # paddings
            feat = _encodeBIOFeature(None, encoding_style)
        else:
            for (tag, entity) in tag_seq[abs_pos]:
                if entity == entity_type:  # first match of entity type
                    feat = _encodeBIOFeature(tag, encoding_style)
                    break
            if feat is None:  # no match of any entity type, or an O tag
                feat = _encodeBIOFeature(tags.NETags.O, encoding_style)
        features.extend(feat)

    return features


def collectDataset(weak_tags, gold_tags, entity_type, window_size, encoding_style):
    """
    Collect a dataset comprises of resulting tags from the ground truth and the weak classifiers.

    :param weak_tags: An iterable of tagging results of weak classifiers, as returned by "load...Tags()" functions.
    :param gold_tags: The gold standard tags, as returned by the loadGoldStandardTags() function
    :param entity_type: A type of entity of interest
    :param window_size: Size of the the sliding window measured by the number of features.
    :param encoding_style: (EncodingStyle enum)
    :return:
    """
    X = list()
    y = list()
    for doc_id in gold_tags:
        num_examples = len(gold_tags[doc_id][KEY_TAGS])
        for feat_id in range(num_examples):
            feat_y = _extractFeatureWindow(gold_tags[doc_id][KEY_TAGS], feat_id, 1, entity_type, EncodingStyle.Number)
            y.extend(feat_y)  # typically a label is an integer

            feat_x = list()
            for weak_classifier in weak_tags:
                feat = _extractFeatureWindow(weak_classifier[doc_id][KEY_TAGS], feat_id, window_size, entity_type, encoding_style)
                feat_x.extend(feat)
            X.append(feat_x)

    return np.array(X), np.array(y)


def trainStackingEnsembler(dataset, algorithm_name):
    """
    Perform stacking ensemble learning.

    :param dataset: As returned by the collectDataset() function.
    :param algorithm_name: (str) Softmax / ECOC

    :return:
    """
    X, y = dataset
    if algorithm_name.lower() == "softmax":
        clf = LogisticRegression(random_state=0, solver="lbfgs", multi_class="multinomial").fit(X, y)
        acc = clf.score(X, y)
    elif algorithm_name.lower() == "ecoc":
        clf = OutputCodeClassifier(LinearSVC(random_state=0), code_size=2, random_state=0).fit(X, y)
        acc = clf.score(X, y)
    else:
        print("Unrecognized algorithm name! Halting.")
        sys.exit(1)

    return clf, acc


def predict(ensemble_classifier, X, entity_type):
    """
    Run prediction on a new dataset, and convert it back to entity B/I/O.

    :param ensemble_classifier: As returned by trainStackingEnsembler()
    :param X: As returned by collectDataset()
    :param entity_type:
    :return:
    """
    prediction = ensemble_classifier.predict(X)
    prediction = [tags.NETags(pred) for pred in prediction]
    prediction = [(tags.createTag(pred) if pred == tags.NETags.O else tags.createTag(pred, entity_type))
                  for pred in prediction]
    return prediction


def joinPrediction(individual_entity_predictions):
    """
    Join predictions on individual entities into joined label prediction.

    :param individual_predictions: A list of predictions on individual entities.
    :return:
    """
    num_entities = len(individual_entity_predictions)  # number of entities being predicted separately
    seq_length = len(individual_entity_predictions[0])  # length of the tag sequence
    for i in range(1, num_entities):
        assert seq_length == len(individual_entity_predictions[i])

    joined_labels = list()
    for label_idx in range(seq_length):
        labels = [individual_entity_predictions[i][label_idx] for i in range(num_entities)]
        unique_labels = set(labels)
        if unique_labels == {'O'}:
            join_label = 'O'
        else:
            # first, remove all 'O' tags
            try:
                while True:
                    labels.remove('O')
            except:
                pass
            # then, concatenate the remaining
            join_label = tags.CONCATENATION_CHAR.join(labels)

        joined_labels.append(join_label)

    return joined_labels


def segmentBackIntoDocuments(joined_predictions, gold_tags):
    """
    The joined prediction tag sequence encapsulate all documents in the dataset.
    This function split it back segments, where each segment correspond to a document.
    :param joined_predictions:
    :param gold_tags:
    :return:
    """
    segmented_predictions = list()
    start = end = 0
    for doc_id in gold_tags:
        num_tags = len(gold_tags[doc_id][KEY_TAGS])
        start = end
        end += num_tags
        segmented_predictions.append(joined_predictions[start:end])

    assert end == len(joined_predictions)
    return segmented_predictions
