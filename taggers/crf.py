"""
Functionality to interface with the Stanford Core NLP package.

@Author: Thanh Thieu
"""

import ios
import csv
import m_utils

from nltk.tag.stanford import *
import shutil

TAB_COULUMN_EXTENSION = ".col"  # tab separated column
ANN_COL_FOLDER = "annotations_col"
TRAIN_SUBFOLDER = "train"
VAL_SUBFOLDER = "val"
TEST_SUBFOLDER = "test"
TRAIN_FILE_IDs = "file_ids.txt"  # List IDs of training files
TRAIN_DATA_FILE = "data.col"  # Combined contents of all training files

MODEl_FILE_NAME = "stanford-crf-model"
MODEL_FILE_EXTENSION = ".ser.gz"
MODEL_FOLDER = "models/crf"


def _modelFile(fold):
    """
    Return a model file name from a fold number.
    :param fold: Fold number.
    :return: (str)
    """
    return MODEl_FILE_NAME + "-" + str(fold) + MODEL_FILE_EXTENSION


def _exportCoNLLFormat(token_tag_pairs, doc_ids, target_folder):
    """
    Export a tagged corpus to tab-separated, column format similar to CoNLL format.
    :param target_folder: (str)
    :param token_tag_pairs:
        A list of lists of pairs of (token, tag).
    :param doc_ids:
        A list of IDs of documents.
    :return: None
    """
    # Check input integrity
    if len(token_tag_pairs) != len(doc_ids):
        raise ValueError("Inconsistent doc IDs vs contents.")

    # Create the target folder if not exist
    folder = os.path.join(ios.DATA_DIR, target_folder)
    m_utils.safe_mkdir(folder)

    # Loop through the corpus and write out files
    combined_f = open(os.path.join(folder, TRAIN_DATA_FILE), 'w', newline='')  # Combine data file
    combined_writer = csv.writer(combined_f, delimiter=" ")
    for doc_idx in range(len(doc_ids)):
        docID = doc_ids[doc_idx]
        contents = token_tag_pairs[doc_idx]
        # Write individual files, at the same time with the combined data file
        individual_file_path = os.path.join(folder, str(docID) + TAB_COULUMN_EXTENSION)
        with open(individual_file_path, 'w', newline='') as individual_f:  # Individual files
            individual_writer = csv.writer(individual_f, delimiter=" ")
            for token_tag in contents:
                individual_writer.writerow(token_tag)
                combined_writer.writerow(token_tag)
        # Separate documents with an empty line in the combined data file
        combined_writer.writerow([])  # Empty line

    combined_f.close()


def _exportFileIDs(doc_ids, file_path):
    """
    Write a list of comma-separated training file names to a text file.
    :param doc_ids:
        A list of IDs of documents.
    :param file_path:
        Path to a text file where the list of document IDs are written.
        Assume the folder path to the output file already exists.
    :return:
    """
    with open(file_path, 'w', encoding='utf-8') as f:
        for docID in doc_ids:
            name = str(docID) + TAB_COULUMN_EXTENSION
            f.write(name)
            f.write(",")


def _exportAllFormat(fold, train_or_test_subfolder, token_tag_pairs, doc_ids, usage_subfolder=''):
    """
    Export data to all formats available, including CoNLL.

    :param usage_subfolder: How to use this data, e.g. conll, crf, etc.
    :param fold: Fold number.
    :param train_or_test_subfolder: Name of train/test subfolder.
    :param token_tag_pairs:
        A list of lists of pairs of (token, tag).
    :param doc_ids:
        A list of IDs of documents.
    :return:
    """
    folder_path = os.path.join(ios.DATA_DIR, ANN_COL_FOLDER, usage_subfolder, str(fold), train_or_test_subfolder)
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
    _exportCoNLLFormat(token_tag_pairs, doc_ids, folder_path)
    _exportFileIDs(doc_ids, os.path.join(folder_path, TRAIN_FILE_IDs))


def train(token_tag_pairs, doc_ids, fold):
    """
    A Stanford CRF model will be trained on a server.
    This function only export training data (to be ported to the server) if a model doesn't exist.
    :param token_tag_pairs:
        A list of lists of pairs of (token, tag).
    :param doc_ids:
        A list of IDs of documents.
    :param fold:
        Fold number.
    :return: None
    """
    model_path = os.path.join(ios.DATA_DIR, MODEL_FOLDER, _modelFile(fold))
    if not os.path.exists(model_path):
        print("No Stanford CFR model exists, exporting training data")
        _exportAllFormat(fold, TRAIN_SUBFOLDER, token_tag_pairs, doc_ids, "conll")
    else:
        print("A CRF model already exist at: {0}".format(str(model_path)))

    return None  # Copy model from server after training there


def tag(test_data, model=None, fold=None):
    """
    Tag the test data using provided model.
    :param test_data:
        A list of (lists of tokens).
    :param model:
        If not None then this is the subfolder containing trained models.
    :return:
        A list of lists of predicted labels.
        Or None if no model found.
    """
    model_path = os.path.join(ios.DATA_DIR, MODEL_FOLDER, model if model else "", _modelFile(fold))
    lib_path = os.path.join(ios.LIB_DIR, "stanford-ner-2018-02-27", "stanford-ner-3.9.1.jar")

    if not os.path.exists(model_path):
        print("Cannot find a Stanford CRF model, no testing")
        return None
    elif not os.path.exists(lib_path):
        print("Cannot find the Stanford NER library, no testing")
        return None
    else:
        print("Tagging with a Stanford CRF model at {}".format(str(model_path)))
        tagger = StanfordNERTagger(model_filename=model_path, path_to_jar=lib_path, encoding='utf8')
        prediction = tagger.tag_sents(test_data)
        predicted_tags = [[tag for (tok, tag) in pred] for pred in prediction]
        return predicted_tags
