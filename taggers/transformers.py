"""
Supplementary processing to run Transformer models.
"""

import os
import json

import config
import m_utils


def _metaFilename(file_name, max_tokens):
    return "{}.maxtokens{}.json".format(file_name, max_tokens)


def _splitMaxTokens(folder_path, max_tokens=512):
    """
    Split the training data into segments of specified maximum number of tokens.
    The reason is some Transformer models have limitation on length of positional encoding.
    :param folder_path: (str or path) A folder to search for all text files.
        Will apply splitting at max_tokens to all text files in the input folder.
    :param max_tokens: (int) Maximum number of tokens allowed in an individual sequence.
    :return: All file contents are split at max_tokens.
        Also create an additional file per each processed file, to record positions of added emply lines.
    """
    print('Split sentences/documents at max_tokens = {}\n'.format(max_tokens))
    for file_name in os.listdir(folder_path):
        if file_name.endswith('.txt'):
            f = open(os.path.join(folder_path, file_name), 'r')
            lines = f.readlines()
            f.close()
            buffer = list()
            length = 0
            split_positions = list()
            for line in lines:
                buffer.append(line)
                if not line.strip():  # empty line
                    length = 0
                else:
                    length = length + 1
                    if length >= max_tokens:  # break the sequence when reach max_tokens
                        buffer.append('\n')  # empty line
                        split_positions.append(len(buffer))
                        length = 0
            # Overwrite with new contents
            f = open(os.path.join(folder_path, file_name), 'w')
            f.write(''.join(buffer))
            f.close()
            # Write splitting positions
            with open(os.path.join(folder_path, _metaFilename(file_name, max_tokens)), 'w') as f:
                json.dump(split_positions, f)


def processDataset():
    for max_len in [512, 256, 128]:
        for mode in ['sent', 'doc']:
            for fold in [1, 2, 3, 4, 5]:
                dataset_path = os.path.join(config.getConfig().DATA_DIR, 'annotations_col',
                                            'conll_max{}'.format(max_len), 'gsc_all_ents', mode, str(fold))
                print('Split {}'.format(dataset_path))
                _splitMaxTokens(dataset_path, max_len)


def _unsplitTags(tag_file, split_metadata_file, output_file):
    """
    Undo the splitting at max num tokens on resulting tag prediction.
    :param tag_file: Predicted entity tags file.
    :param split_metadata_file: Meta data file created when the original text file was split.
    :param output_file: Output tag file.
    :return:
    """
    try:
        with open(tag_file, 'r') as f:
            tags = f.readlines()
    except:
        print("Unsplit transformer's tags: Cannot open tag file: {}".format(tag_file))
        return

    try:
        with open(split_metadata_file, 'r') as f:
            meta = json.load(f)
    except:
        print("Unsplit transformer's tags: Cannot open meta file: {}".format(split_metadata_file))
        return

    buffer = list()
    prev_pos = 0
    for split_pos in meta:
        if split_pos > len(tags):
            print("Unsplit transformer's tags: Tag prediction incomplete. Meta pos = {} ; Tag sequence length = {}".format(split_pos, len(tags)))
            print("    Tag file: {}".format(tag_file))
            print("    Meta file: {}".format(split_metadata_file))
            return
        if tags[split_pos - 1] != '\n':
            raise AssertionError('Inconsistent meta slit position!')
        buffer.extend(tags[prev_pos:split_pos - 1])
        prev_pos = split_pos
    # take the remaining after the last splitting point
    buffer.extend(tags[prev_pos:])

    if len(tags) - len(buffer) != len(meta):
        raise AssertionError('Transformer unsplit tags: Inconsistent sizes!')
    with open(output_file, 'w') as f:
        f.write(''.join(buffer))


def processTags():
    for model in ['bert_large_cased', 'biobert', 'bert_base_cased']:
        # for model in ['bert_base_uncased']:
        for token_limit in [256]:
            # for token_limit in [512, 256, 128]:
            for entity_types in ['gsc_all_ents']:
                for style in ['sent', 'doc']:
                    # for style in ['sent']:
                    for fold in [1, 2, 3, 4, 5]:
                        # for fold in [1]:
                        for cross in ['test', 'valid']:
                            # for cross in ['test']:
                            meta_folder = os.path.join(config.getConfig().DATA_DIR, 'annotations_col', "conll_max{}".format(token_limit),
                                                       entity_types, style, str(fold))
                            tag_folder = os.path.join(config.getConfig().DATA_DIR, "models/transformers", model, "max{}".format(token_limit),
                                                      entity_types, style, str(fold))
                            output_folder = os.path.join(config.getConfig().DATA_DIR, 'annotations_col', model,
                                                         "max{}".format(token_limit), entity_types, style, str(fold))
                            m_utils.safe_mkdir(output_folder)
                            _unsplitTags(os.path.join(tag_folder, "{}_predictions.txt".format(cross)),
                                         os.path.join(meta_folder, _metaFilename("{}.txt".format(cross), token_limit)),
                                         os.path.join(output_folder, "{}.txt".format(cross)))


def tag(model, style, fold, cross, ref_tokens):
    """
    Read predicted tags from a file.
    :param model:
    :param style:
    :param fold:
    :param cross:
    :param ref_tokens: To verify the tags are correctly aligned with tokens.
    :return:
    """
    tag_file = os.path.join(config.getConfig().DATA_DIR, config.getConfig().ANN_COL_FOLDER, model, style, str(fold), "{}.txt".format(cross))
    try:
        with open(tag_file, 'r') as f:
            results = f.readlines()
    except:
        print("Cannot read Transformer tag prediction from: {}".format(tag_file))
        return None

    predicted_tags = list()
    sent_tags = list()
    sent_idx = 0
    tok_idx = -1
    for line in results:
        line = line.strip()
        if not line:  # new line character; signify end of a sentence
            if len(sent_tags) != len(ref_tokens[sent_idx]):
                raise ValueError('Transformer sentence not aligned with reference tokens sentence!')
            predicted_tags.append(sent_tags)
            sent_tags = list()
            sent_idx += 1
            tok_idx = -1
        else:
            tok, label = line.split(' ')
            tok_idx += 1
            if tok != ref_tokens[sent_idx][tok_idx]:
                print("Warning: Transformer token text '{}' not aligned with reference token text '{}' !".
                      format(tok, ref_tokens[sent_idx][tok_idx]))
            sent_tags.append(label)
    return predicted_tags


def main():
    # processDataset()
    processTags()


if __name__ == '__main__':
    main()
