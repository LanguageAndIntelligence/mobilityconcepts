"""
Author: Thanh Thieu

Facilities to pair tokens with tags for sequence modeling.
"""

from enum import Enum

import m_utils
import integrity


class NETags(Enum):
    """
    Sequence tags types for named entities.
    """
    B = 0  # Begin
    I = 1  # Inside
    O = 2  # Outside


def createTag(tag_type, entity_type=None):
    """
    Create a tag string based on the type of entity, and the type of tag.

    :param tag_type:
        A value of NETags.
    :param entity_type:
        Subclass of Entity
    :return:
        str
    """
    if entity_type is None:
        return tag_type.name
    else:
        return tag_type.name + '-' + entity_type.__name__


CONCATENATION_CHAR = '-'  # character used to concatenate multiple tags


def concatenateTags(tag1, tag2):
    """
    Concatenate two tag strings to create a conbined tag string.
    :param tag1: (str)
    :param tag2: (str)
    :return: (str)
    """
    if tag1:
        return tag1 + CONCATENATION_CHAR + tag2
    else:
        return tag2


def pairTokensBIOTags(doc, entity_types, tokenizer, text_preprocessor=None):
    """
    Pair each token with a corresponding B/I/O tag.

    :param doc: (Document)
        A document containing both raw text and annotated entities.
    :param entity_types: (list of entity types)
        List types of entities to extract tags from.
    :param tokenizer: (module)
        A word tokenizer, preferably one that implements the Penn treebank tokenizer.
        Has to expose a "span_tokenize" API.
    :param text_preprocessor: (function)
        A function to pre-process text before tokenization.
    :return: (zip(tokens, tags), tags, tokens, token_spans)
        Iteration of pairs of (token, tag)'s, together with separate lists of tokens and tags.
    """
    text = doc.text
    if text_preprocessor:
        text = text_preprocessor(text)

    tokens, token_spans, sent_bounds = tokenizer.span_tokenize(text)
    tags = [None] * len(tokens)

    # Put B-tags and I-tags in positions engulfed by entities
    for ent_type in entity_types:
        ents = doc.getAnnotations(ent_type)
        for ent in ents:
            # Find tokens engulfed by this entity
            for begin_token_idx in range(len(token_spans)):
                if token_spans[begin_token_idx][0] >= ent.first_char_index:
                    break
            for end_token_idx in range(begin_token_idx, len(token_spans)):
                if token_spans[end_token_idx][1] > ent.last_char_index:
                    break
            # Check integrity
            integrity.checkEntityTokens(doc.docID, ent, tokens[begin_token_idx:end_token_idx],
                                        token_spans[begin_token_idx:end_token_idx])
            # Augment tags of enclosed tokens by this entity type
            tags[begin_token_idx] = concatenateTags(tags[begin_token_idx], createTag(NETags.B, ent_type))
            for token_idx in range(begin_token_idx + 1, end_token_idx):
                tags[token_idx] = concatenateTags(tags[token_idx], createTag(NETags.I, ent_type))

    # Put O-tags in left-over positions not claimed by any entity
    for idx, val in enumerate(tags):
        if not val:
            tags[idx] = createTag(NETags.O)

    return list(zip(tokens, tags)), tags, tokens, token_spans, sent_bounds


def parseTag(tag_str):
    """
    Parse a tag string into B/I/O tags paired with entity types.

    :param tag_str:
        A string representation of the combined tag.
    :return:
        A list of pairs of (tag type, entity type).
    """
    toks = tag_str.split(CONCATENATION_CHAR)
    assert len(toks) == 1 or len(toks) % 2 == 0, "Double check syntax of tag concatenation!"
    tags = list()
    for idx in range((len(toks) + 1) // 2):
        tag_value = m_utils.lookupAttributeValue(toks[2 * idx], NETags)
        entity_value = m_utils.lookupEntityType(toks[2 * idx + 1]) if 2 * idx + 1 < len(toks) else None
        tags.append((tag_value, entity_value))
    return tags


def coalesceEntiyBIOTags(token_spans, parsed_tags):
    """
    Combine B/I/O tags into text spans of entities.

    :param token_spans:
        A list of tuple (first char index, last char index) of tokens.
    :param parsed_tags:
        A list of list of parsed tuples (B/I/O tag, entity type) corresponding to the token spans.
    :return:
        A list of tuples (first char index, last char index, entity type).
    """
    entity_spans = list()
    for i in range(len(parsed_tags)):
        tags_at_index = parsed_tags[i]
        for (tag_value, entity_type) in tags_at_index:
            if tag_value == NETags.B:
                for k in range(i + 1, len(parsed_tags)):
                    tags_at_sub_index = parsed_tags[k]
                    found = False
                    # ASSUMPTION: sub-entities of same type do NOT overlap
                    for (sub_tag_value, sub_entity_type) in tags_at_sub_index:
                        if sub_entity_type == entity_type and sub_tag_value == NETags.I:
                            found = True
                            break  # This token is a continuation I of the B tag
                    if not found:
                        k = k - 1
                        break  # This token does NOT continue the B tag
                entity_spans.append((token_spans[i][0], token_spans[k][1], entity_type))
    return entity_spans
